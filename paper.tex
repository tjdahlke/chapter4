
\chapter{Interpreter guidance for inclusion discovery}
\label{chap:chap4}


% \par Regardless of the parameterization used to describe the implicit surface, the way its shape is initialized can influence the inversion outcome. 


% \par One challenge in salt model inversion is creating models that account for inclusions. We can modify the level set gradient described earlier to allow updating in regions other than the current salt boundary, such as the salt interior, where inclusions may exist. This requires a likelihood map from an interpreter showing where a salt boundary is thought to exist. We can further leverage that information to initialize the implicit surface to increase update sensitivity in those regions, quickening inversion. 

\par In this chapter I explain how one can modify the implementation of the level set inversion  so that outside input can elegantly guide the inversion by means of expanding the footprint of the gradient update and by intelligently initializing the implicit surface. This improves the overall convergence rate, which is important because the operators used to find the search direction are computationally expensive. I finish by comparing inversion examples that showcase how these types of guidance can improve the convergence rate and allow for model updating that the unmodified level set algorithm is unable to replicate.


% Motivation
    % * Support limitations inherent in original derivation
    % * Even with expanded support, the implicit surface still guides sensitivity
    % * Robustness of workflow
    
% 2D examples
    % Compare: Steepest Descent
    % Compare: GN Hessian
        
\section{Motivation}
    

\par An advantage of the level set framework is that the implicit surface can be deformed at positions that are not on the current boundary, even to the extent that it `punctures' the zero level set and creates a `donut hole' or other topology. This can be useful for modifying salt bodies, since we may begin with a solid salt body, and then later deform the implicit surface such that the salt has inclusions inside it. 

\par However, an underlying assumption of the gradient derived earlier is that the salt model only changes at the boundary of the original shape. This is because the salt is defined using a Heaviside function approximation, and so the gradient contains a $\delta(\phi_{0})$ term that (in a practical sense) updates at the boundary only. If we only update along the current boundary in this manner, we stifle the possibility of topology changes, such as the discovery of inclusions that exist away from the current boundary. This is an inherent limitation of the theory I have derived thus far.

\par Furthermore, I have yet to discuss any methodology for initializing the implicit surface prior to inversion other than stipulating that the zero crossing position correlates to the boundary of the salt body with which we wish to begin. As long as the interior salt region is greater than zero, the Heaviside function indicates it as salt. This means that beyond the requirement of having the proper sign, there is flexibility in how one initializes that surface. I leverage this flexibility to create regions of preferential updating sensitivity.

\section{Modifications to gradient}

\par Because the support of the gradient I previously derived is a subset of the actual objective function support (which is the entire model domain), I can expand the support of the masking term to preserve regions inside the current salt boundaries (possible inclusion regions) without affecting our ability to minimize our objective function. This means I can modify the masking term in our $\mathbf{D}$ operator to allow for updating outside of the boundary in regions selected by seismic interpreter guidance. I represent this modification with a new term $\boldsymbol{\hat{\delta}(\phi_{0}},\mathbf{G})$, which takes into account the interpreter guidance $\mathbf{G}$:
\begin{align*}
    \mathbf{D} &=  \begin{bmatrix}
    \boldsymbol{\hat{\delta} (\phi_{0},G)(c_{\text{salt}} - b_{0})} &
    \boldsymbol{I - \hat{H}(\phi_{0})}
    \end{bmatrix} .
\end{align*}

\noindent The $\mathbf{G}$ term defines additional regions of our model where one wishes to allow updating to occur, for example, areas where an interpreter suspects inclusions to exist.

\section{Initializing the implicit surface using interpreter guidance}

\par However, even if I expand the footprint of the region where updating is allowed, I can only discover an inclusion if the update perturbs the implicit surface below the zero-level set. This means the initial height of the implicit surface affects sensitivity to updating in different regions. For example, in Figures \ref{fig:inclusion-discovery1a} and \ref{fig:inclusion-discovery1b}, we can see how an unguided implicit surface can receive an update that suggests an inclusion, but not realize that change in the resulting salt model. 


\par However, I can initialize $\phi$ as shown in Figure \ref{fig:inclusion-discovery2a} in order to increase sensitivity in that area of the salt model. This time, the same update is able to create the inclusion in the same iteration (\ref{fig:inclusion-discovery2b}). This is especially important when there are other model regions being updated that have a high impact on the data residual. In the case of Figure \ref{fig:inclusion-discovery1a}, the linesearch that chooses how strongly to apply the update will be most influenced by regions (like the top of salt) to which the objective function is inherently more sensitive, and may never allow for an inclusion update deeper in the model, even after many iterations.

% Unguided implicit surface updating
\multiplot{2}{inclusion-discovery1a,inclusion-discovery1b}{width=0.5\columnwidth}{a) Example of salt model before update applied, and b) after update applied to simple $\phi$ surface with no sensitivity preferences. Note that the update applied in Figure \ref{fig:inclusion-discovery1a} does not decrease the implicit surface below zero in Figure \ref{fig:inclusion-discovery1b}, resulting in no salt boundary change. \NR}

% Guided implicit surface updating
\multiplot{2}{inclusion-discovery2a,inclusion-discovery2b}{width=0.5\columnwidth}{a) Example of salt model before update applied, and b) after update applied to $\phi$ surface with sensitivity preferences already incorporated into initial $\phi$ surface. Note that the update applied in Figure \ref{fig:inclusion-discovery2a} decreases the implicit surface below zero in Figure \ref{fig:inclusion-discovery2b}, resulting in a salt boundary change.\NR}



\par The opportunity here is to set the height of the implicit surface according to how likely we believe that an inclusion is present at that position. This allows us to input a probabilistic mapping of inclusion likelihood into our initialization of $\phi$. The same interpreter input $G$ from before can be scaled and used to modify the implicit surface height appropriately. For the Sigsbee model example, Figure \ref{fig:initialphi-fullyGuided-sigsbee} shows the implicit surface with interpreter guidance applied, and Figure \ref{fig:initialphi-unguided-sigsbee} shows it without. In Figure \ref{fig:initialphi-fullyGuided-sigsbee}, we can see light-colored regions of the implicit surface that are not present in Figure \ref{fig:initialphi-unguided-sigsbee}, and have a lower value than the rest of the interior salt region. This means updates in this area will be more likely to break past the zero-level set contour and change the value of $\hat{H}(\phi_{o})$ to create an inclusion in the model (if the data suggests that). In the case of erroneous interpreter guidance, the gradient may not create an inclusion, or if it does, it may fill it back in during later iterations.

\plot{initialphi-fullyGuided-sigsbee}{width=5in}{Initial implicit surface (guided). Note the three light colored regions where the implicit surface value has been reduced. \ER}
\plot{initialphi-unguided-sigsbee}{width=5in}{Initial implicit surface (unguided). Note the lack of the three light colored regions that are present in Figure \ref{fig:initialphi-fullyGuided-sigsbee}.\ER}


% \section{Demonstration on 2D Sigsbee model}
% \par I used a portion of the 2D Sigsbee velocity model and modified it with a low-velocity inclusion (Figure \ref{fig:truemodel}). Our initial model was the same expect with the inclusion shifted left about 1.5km (Figure \ref{fig:initmodel-guided}). For each inversion the acquisition was 24 shots and 380 receivers evenly spaced, with a 8Hz source wavelet. The first inversion had an implicit surface initialized using interpreter guidance, while the second did not. Both inversions used the modified masking function ($\hat{\delta}(\phi_{0},G)$) to allow for updating inside the salt body, and used the same RBF centers (Figure \ref{fig:centers}). However, the `unguided' inversion did not have a modified implicit surface. In both cases, updating was done only on the $\lambda$ parameter; the $b$ parameter (background velocity) was fixed over all iterations.

% \plot{truemodel-sigsbee}{width=5in}{True model\ER}
% \plot{centers}{width=5in}{RBF center locations.\ER}

% \par As expected, by expanding the support of the gradient to include the interpreter guidance areas, I am able to recover the correct inclusion and close the false one (Figure \ref{fig:deltaM-guided8}) for both cases. Even with the some of the interpreter guidance being incorrect (compare Figure \ref{fig:initphi-guided} with \ref{fig:truemodel}), the inversion was not misguided. Further, I found that the additional step of initializing $\phi$ as per Figure \ref{fig:initphi-guided} gives better convergence in terms of both the model and data residual norms (Figure \ref{fig:data-norm} and \ref{fig:model-norm}).


% \plot{initialmodel-fullyguided-sigsbee}{width=5in}{Initial model (guided).\ER}
% \plot{deltaM-guided8}{width=5in}{$m_{\text{true}}-m_{\text{init}}$ (guided, at it=8)\CR}

% % Norm curves
% \plot{data-norm}{width=5in}{Objective function value (data residual norm).\CR}
% \plot{model-norm}{width=5in}{Model residual norm.\CR}





\section{Demonstration on a Gulf of Mexico model}

\par In this 2D synthetic example, I use a model where the inclusion is close enough to the boundary (Figure \ref{fig:truemodel-gom}) that there is a reasonable chance of the unmodified level set approach finding the inclusion without any of the interpreter guidance methods already described. Beginning with a model that has no inclusion (Figure \ref{fig:startingmodel-gom}), I compare the unmodified method against the partially guided inversion (expanded gradient only) and the fully guided inversion (expanded gradient and implicit surface initialization). For both of the guided approaches, I expand the gradient by modifying the masking to allow for updating in the zone where I anticipate that an inclusion exists (Figure \ref{fig:initialphi-fullyGuided-gom}). The unguided approach has no expanded gradient (Figure \ref{fig:initialphi-unguided-gom}). In the fully guided example, I also initialize the implicit surface using the same interpreter guidance so that it has a lower value where I anticipate an inclusion (Figure \ref{fig:guidedMask-gom}), while the unguided and partially guided approaches do not use this guidance for initialization of the implicit surface (Figure \ref{fig:unguidedMask-gom}).

\multiplot{2}{truemodel-gom,startingmodel-gom}{width=0.9\columnwidth}{True model a) and starting model b) and used for all tests. \ER}

\multiplot{2}{initialphi-fullyGuided-gom,initialphi-unguided-gom}{width=0.9\columnwidth}{Implicit surface with interpreter guidance initialization a) and without it b). \ER}

\multiplot{2}{guidedMask-gom,unguidedMask-gom}{width=0.9\columnwidth}{Masking term ($\hat{\delta}(\phi_{0},G)$) for guided gradient methods (a), and masking term ($\delta(\phi_{0})$) for the unguided approach (b). \ER}

\par As expected, I find that the guided inversion approaches perform much better (Figures \ref{fig:gom-guidanceVsnoguidance-dataNorm} and \ref{fig:gom-guidanceVsnoguidance-modelNorm}) in terms of reducing both the model residual and data residual norms. In the partially guided approach, the data guides the inversion to the correct inclusion shape regardless by means of the expanded gradient (see Figure \ref{fig:model-15-partiallyGuided}. However, the implicit surface initialization used in the fully guided approach (Figure \ref{fig:model-15-fullyGuided}) further improves the convergence rate. Meanwhile, the unmodified method begins to approach the true answer only after 65 iterations (Figure \ref{fig:model-65-unguided}). In a case where the true inclusion position is further from the starting salt boundary, I would likely have an even worse result using the unmodified approach.

\plot{gom-guidanceVsnoguidance-dataNorm}{width=5in}{Data residual norm for level set inversion with unmodified approach (dark blue), partially guided approach (light blue), and fully guided approach (yellow). \CR}

\plot{gom-guidanceVsnoguidance-modelNorm}{width=5in}{Model residual norm for level set inversion with unmodified approach (dark blue), partially guided approach (light blue), and fully guided approach (yellow). \CR}

\multiplot{3}{model-15-unguided,model-15-partiallyGuided,model-15-fullyGuided}{width=0.6\columnwidth}{Inverted model after 15 steepest descent iterations using unguided approach a), partially guided approach b), and fully guided approach c). \CR}

\plot{model-65-unguided}{width=5in}{Inverted model after 65 steepest descent iterations using unmodified approach. \CR}



\section{Conclusions}
I discuss the limitations of the standard level set formulation and how it constrains our ability to create salt models with inclusions. I then introduce the idea of using a likelihood map generated by interpreters as guidance for the inversion. This guidance is used to expand the level set gradient footprint, as well as initialize the implicit surface to create areas of higher sensitivity to updating. Using 2D synthetic examples, I show how the expanded gradient and the implicit surface initialization both contribute to speeding up the convergence.



