program oper2

  use sep
  use mem_mod
  use fbi_mod

  implicit none

  integer                             :: nt, n1, n2, wfld_j, i
  integer                             :: sou_nx, sou_ox, sou_dx, sou_z, sou_x, isou
  integer                             :: rec_nx, rec_ox, rec_dx, rec_z, rec_x, irec
  integer                             :: bc1, bc2, bc3, bc4, nh, oh
  real                                :: dt, d1, o1, d2, o2
  integer                             :: oper
  logical                             :: adj, verbose
  real, dimension(:), allocatable     :: wavelet
  real, dimension(:,:), allocatable   :: vel, deltaB
  real, dimension(:,:,:), allocatable :: refl, source, data, deltaD, deltaI,srcwave_in
  integer, dimension(8)               :: timer0, timer1
  real                                :: timer_sum=0.

  call sep_init()

!---------------------------------------------------------------------------
  call DATE_AND_TIME(values=timer0)
!---------------------------------------------------------------------------

  call from_param("isou", isou)
  call from_param("verbose", verbose,.false.)

  call from_param("oper", oper)
  if(oper < 0 .or. oper > 3) call erexit("oper value is not correct")

  call from_history("n1", n1)
  call from_history("o1", o1)
  call from_history("d1", d1)
  call from_history("n2", n2)
  call from_history("o2", o2)
  call from_history("d2", d2)

  call from_param("nh", nh, 1)
  call from_param("oh", oh, 0)
  if(nh<1-oh) write(0,*) "WARNING: zero subsurface axis is not included"

  call from_param("nt", nt)
  call from_aux("wavelet", "d1", dt)

  call from_param("bc1", bc1)
  call from_param("bc2", bc2)
  call from_param("bc3", bc3)
  call from_param("bc4", bc4)
  call from_param("wfld_j", wfld_j)

  call from_param("sou_nx", sou_nx)
  call from_param("sou_ox", sou_ox)
  call from_param("sou_dx", sou_dx)
  call from_param("sou_z", sou_z)
  if(sou_ox < 1 .or. (sou_nx-1)*sou_dx+sou_ox > n2) call erexit("sou axis values are not correct")
  if(sou_z < 1 .or. sou_z > n1) call erexit("sou_z value is not correct")

  call from_param("rec_nx", rec_nx)
  call from_param("rec_ox", rec_ox)
  call from_param("rec_dx", rec_dx)
  call from_param("rec_z", rec_z)
  if(rec_ox < 1 .or. (rec_nx-1)*rec_dx+rec_ox > n2) call erexit("rec axis values are not correct")
  if(rec_z < 1 .or. rec_z > n1) call erexit("rec_z value is not correct")

  call from_param("adj", adj)
  if (verbose) write(0,*) ">>>>>>>>>>>>>>>>    Initializing 1,2,3 <<<<<<<<<<<<<<"
  call init1(n1, o1, d1, n2, o2, d2, bc1, bc2, bc3, bc4)
  call init2(sou_nx, sou_ox, sou_dx, sou_z, rec_nx, rec_ox, rec_dx, rec_z)
  call init3(nt, dt, wfld_j, nh, oh)

  if (verbose) write(0,*) ">>>>>>>>>>>>>>>>    Writing history files <<<<<<<<<<<"
  if((oper .eq. 0) .or. (oper < 3 .and. .not. adj)) then
    call to_history("n1", nt)
    call to_history("o1", 0.)
    call to_history("d1", dt)
    call to_history("n2", rec_nx)
    call to_history("o2", rec_ox)
    call to_history("d2", rec_dx)
    ! call to_history("n3", sou_nx)
    ! call to_history("o3", sou_ox)
    ! call to_history("d3", sou_dx)
  else
    call to_history("n1", n1)
    call to_history("o1", o1)
    call to_history("d1", d1)
    call to_history("n2", n2)
    call to_history("o2", o2)
    call to_history("d2", d2)
    if((oper .eq. 1) .or. (oper .eq. 3 .and. .not. adj))then
      call to_history("n3", nh)
      call to_history("o3", oh)
      call to_history("d3", 1.)
    end if
  end if

  if (verbose) write(0,*) ">>>>>>>>>>>>>>>> Prepping wavlet and vel <<<<<<<<<<<<"
  call mem_alloc1d(wavelet, nt)
  call mem_alloc2d(vel, n1, n2)

  call sep_read(vel)
  call sep_read(wavelet, "wavelet")

  call init_alloc(wavelet)
  call model_update(vel)


  if (exist_file('srcwave_in')) then
    allocate(srcwave_in(n1,n2,nt))
    call sep_read(srcwave_in, "srcwave_in")
    call init_srcwave(srcwave_in)
  endif







  if(oper .eq. 0) then
    if (verbose) write(0,*) ">>>>>>>>>>>>>>>> Starting NL MODELING <<<<<<<<<<<<<"
    ! call mem_alloc3d(source, nt, rec_nx, sou_nx)
    ! call mem_alloc3d(data, nt, rec_nx, sou_nx)
    call mem_alloc3d(source, nt, rec_nx, 1)
    call mem_alloc3d(data, nt, rec_nx, 1)
    if(adj) then
      ! do i=1,sou_nx
        ! call sep_read(data(:,:,i), "data")
        call sep_read(data(:,:,1), "data")
      ! end do
      call L_nlop2(adj, .false., source, data, isou)
      ! do i=1,sou_nx
      !   call sep_write(source(:,:,i))
        call sep_write(source(:,:,1))
      ! end do
    else
      ! do isou=1,sou_nx
        sou_x = (isou-1)*sou_dx + sou_ox
        irec = (sou_x-rec_ox)/rec_dx + 1
        ! source(:,irec,isou) = wavelet

        source(:,irec,1) = wavelet
      ! end do

      call L_nlop2(adj, .false., source, data, isou)

      ! do i=1,sou_nx
      !   call sep_write(data(:,:,i))
          call sep_write(data(:,:,1))
      ! end do
    end if
  else if(oper .eq. 1) then
    if (verbose) write(0,*) ">>>>>>>>>>>>>>>> Starting BORN MODELING <<<<<<<<<<<"
    call mem_alloc3d(refl, n1, n2, nh)
    call mem_alloc3d(data, nt, rec_nx, 1)
    ! call mem_alloc3d(data, nt, rec_nx, sou_nx)
    if(adj) then
      ! do i=1,sou_nx
      !   call sep_read(data(:,:,i), "data")
          call sep_read(data(:,:,1), "data")
      ! end do
      call L_lop2(adj, .false., refl, data, isou)
      do i=1,nh
        call sep_write(refl(:,:,i))
      end do
    else
      do i=1,nh
        call sep_read(refl(:,:,i), "refl")
      end do
      call L_lop2(adj, .false., refl, data, isou)
      ! do i=1,sou_nx
      !   call sep_write(data(:,:,i))
          call sep_write(data(:,:,1))
      ! end do
    end if
  else if(oper .eq. 2) then
    if (verbose) write(0,*) ">>>>>>>>>>>>>>>> Starting TOMO MODELING <<<<<<<<<<<"
    call mem_alloc3d(refl, n1, n2, nh)
    call mem_alloc2d(deltaB, n1, n2)
    ! call mem_alloc3d(deltaD, nt, rec_nx, sou_nx)
    call mem_alloc3d(deltaD, nt, rec_nx, 1)
    do i=1,nh
      call sep_read(refl(:,:,i), "refl")
    end do
    call refl_update(refl)
    if(adj) then
      ! do i=1,sou_nx
      !   call sep_read(deltaD(:,:,i), "deltaD")
          call sep_read(deltaD(:,:,1), "deltaD")
      ! end do
      call T_lop2(adj, .false., deltaB, deltaD, isou)
      call sep_write(deltaB)
    else
      call sep_read(deltaB, "deltaB")
      call T_lop2(adj, .false., deltaB, deltaD, isou)
      ! do i=1,sou_nx
      !   call sep_write(deltaD(:,:,i))
          call sep_write(deltaD(:,:,1))
      ! end do
    end if
  else if(oper .eq. 3) then
    if (verbose) write(0,*) ">>>>>>>>>>>>>>>> Starting WEMVA MODELING <<<<<<<<<<"
    ! call mem_alloc3d(data, nt, rec_nx, sou_nx)
    call mem_alloc3d(data, nt, rec_nx, 1)
    call mem_alloc2d(deltaB, n1, n2)
    call mem_alloc3d(deltaI, n1, n2, nh)
    if (verbose) write(0,*) ">>>>>>>>>>>>>>>> WEMVA MODELING 1 <<<<<<<<<<<<<<<<<"

    ! do i=1,sou_nx
    !   call sep_read(data(:,:,i), "data")
        call sep_read(data(:,:,1), "data")
    if (verbose) write(0,*) ">>>>>>>>>>>>>>>> WEMVA MODELING 2 <<<<<<<<<<<<<<<<<"

    ! end do
    call data_update(data)
    if (verbose) write(0,*) ">>>>>>>>>>>>>>>> WEMVA MODELING 3 <<<<<<<<<<<<<<<<<"

    if(adj) then
      if (verbose) write(0,*) ">>>>>>>>>>>>>>>> WEMVA ADJOINT <<<<<<<<<<<<<<<<<<"
      do i=1,nh
        call sep_read(deltaI(:,:,i), "deltaI")
      end do
      call W_lop2(adj, .false., deltaB, deltaI, isou)
      call sep_write(deltaB)
    else
      if (verbose) write(0,*) ">>>>>>>>>>>>>>>> WEMVA FORWARD <<<<<<<<<<<<<<<<<<"
      call sep_read(deltaB, "deltaB")
      call W_lop2(adj, .false., deltaB, deltaI, isou)
      do i=1,nh
        call sep_write(deltaI(:,:,i))
      end do
    end if
  end if

!---------------------------------------------------------------------------
  call DATE_AND_TIME(values=timer1)
  timer0 = timer1-timer0
  timer_sum = timer_sum + (((timer0(3)*24+timer0(5))*60+timer0(6))*60+timer0(7))*1000+timer0(8)
  call to_history("timer sum (sec)", timer_sum/1000)
  call to_history("timer sum (min)", timer_sum/1000/60)
  write(0,*) "timer sum (sec)", timer_sum/1000
  write(0,*) "timer sum (min)", timer_sum/1000/60
  write(0,*) "DONE!"
!---------------------------------------------------------------------------

  ! deallocate(data,model)

end program
