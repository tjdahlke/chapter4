extern "C" {
void laplacianFOR3d(int n, int n1, int n12, float *coef,float *v2dt,float *abc, float *prev,float *cur,float *next,float *nextF);
}

extern "C" {
void laplacianADJ3d(int n, int n1, int n12, float *coef,float *v2dt,float *abc, float *prev,float *cur,float *next,float *nextF);
}