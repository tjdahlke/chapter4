
! >>>>>   CAN ONLY TAKE SINGLE SUBSURFACE OFFSET REFLECTION MODELS

program GN_HESSIAN

  use sep
  use mem_mod
  use fbi_mod

  implicit none
  integer                             :: nt, n1, n2, wfld_j, i, nvb, nsb, npc, np
  integer                             :: sou_nx, sou_ox, sou_dx, sou_z, sou_x, isou
  integer                             :: rec_nx, rec_ox, rec_dx, rec_z, rec_x, irec
  integer                             :: bc1, bc2, bc3, bc4, nh, oh
  real                                :: dt, d1, o1, d2, o2, eigenshift
  integer                             :: oper
  logical                             :: adj, verbose
  real, dimension(:), allocatable     :: wavelet, sparsemodel, sparsedata
  real, dimension(:,:), allocatable   :: vel, input, gnphi, finalvel, modboth
  real, dimension(:,:,:), allocatable :: source, data, deltaD, datatmp, srcwave_in
  integer, dimension(8)               :: timer0, timer1
  real                                :: timer_sum=0.

  ! ---------------------------------------------------------------------------
  call sep_init()
  call DATE_AND_TIME(values=timer0)

  !---------------------------------------------------------------------------
  call from_param("isou", isou)
  call from_param("verbose", verbose,.false.)
  call from_history("n1", n1)
  call from_history("o1", o1)
  call from_history("d1", d1)
  call from_history("n2", n2)
  call from_history("o2", o2)
  call from_history("d2", d2)
  call from_param("nh", nh, 1)
  call from_param("oh", oh, 0)
  if(nh<1-oh) write(0,*) "WARNING: zero subsurface axis is not included"
  call from_param("nt", nt)
  call from_aux("wavelet", "d1", dt)
  call from_param("bc1", bc1)
  call from_param("bc2", bc2)
  call from_param("bc3", bc3)
  call from_param("bc4", bc4)
  call from_param("wfld_j", wfld_j)
  call from_param("sou_nx", sou_nx)
  call from_param("sou_ox", sou_ox)
  call from_param("sou_dx", sou_dx)
  call from_param("sou_z", sou_z)
  if(sou_ox < 1 .or. (sou_nx-1)*sou_dx+sou_ox > n2) call erexit("sou axis values are not correct")
  if(sou_z < 1 .or. sou_z > n1) call erexit("sou_z value is not correct")
  call from_param("rec_nx", rec_nx)
  call from_param("rec_ox", rec_ox)
  call from_param("rec_dx", rec_dx)
  call from_param("rec_z", rec_z)
  if(rec_ox < 1 .or. (rec_nx-1)*rec_dx+rec_ox > n2) call erexit("rec axis values are not correct")
  if(rec_z < 1 .or. rec_z > n1) call erexit("rec_z value is not correct")

  if (verbose) write(0,*) ">>>>>>>>>>>>>>>> Initializing 1,2,3 <<<<<<<<<<<<<<<<<"
  call init1(n1, o1, d1, n2, o2, d2, bc1, bc2, bc3, bc4)
  call init2(sou_nx, sou_ox, sou_dx, sou_z, rec_nx, rec_ox, rec_dx, rec_z)
  call init3(nt, dt, wfld_j, nh, oh)

  if (verbose) write(0,*) ">>>>>>>>>>>>>>>> Prepping wavelet and vel <<<<<<<<<<<"
  call mem_alloc1d(wavelet, nt)
  call mem_alloc2d(vel, n1, n2)
  call sep_read(vel,"velmod")
  call sep_read(wavelet, "wavelet")
  call init_alloc(wavelet)
  call model_update(vel)

  if (verbose) write(0,*) ">>>>>>>>>>>>>>>> Prep for GN comp  <<<<<<<<<<<<<<<<<<"
  call mem_alloc2d(gnphi, n1, n2)
  call mem_alloc2d(finalvel, n1, n2)
  call mem_alloc3d(datatmp, nt, rec_nx, 1)
  allocate(modboth(n1,n2))

  ! ! Read in WEMVA --------------------------------------------------------------
  ! if (verbose) write(0,*) ">>>>>>>>>>>>>>>> Prep for WEMVA comp <<<<<<<<<<<<<<<<"
  ! call mem_alloc3d(data, nt, rec_nx, 1)
  ! ! call mem_alloc2d(deltaB, n1, n2)
  ! call sep_read(data(:,:,1), "data")
  ! call data_update(data)

  if (verbose) write(0,*) ">>>>>>>>>>>>>>>> Writing history files <<<<<<<<<<<<<<"
  call to_history("n1", n1)
  call to_history("o1", o1)
  call to_history("d1", d1)
  call to_history("n2", n2)
  call to_history("o2", o2)
  call to_history("d2", d2)

  ! deltaB=0.0

  !-----------------------------------------------------------------------------
  if (verbose) write(0,*) ">>>>>>>>>>>>>>>> Read input <<<<<<<<<<<<<<<<<<<<<<<<<"
  call sep_read(modboth)



  !-----------------------------------------------------------------------------
  if (verbose) write(0,*) ">>>>>>>>>>>>>>>> Read srcwave <<<<<<<<<<<<<<<<<<<<<<<"
  if (exist_file('srcwave_in')) then
    allocate(srcwave_in(n1,n2,nt))
    call sep_read(srcwave_in, "srcwave_in")
    call init_srcwave(srcwave_in)
  endif


  !-----------------------------------------------------------------------------
  if (verbose) write(0,*) ">>>>>>>>>>>>>>>> BORN FORWARD <<<<<<<<<<<<<<<<<<<<<<<"
  if (verbose) write(0,*) ">>>>>>>>>>>>>>>> sum(inputmodel) = ", sum(modboth)
  call L_lop2(.false., .false., modboth, datatmp, isou)
  if (verbose) write(0,*) ">>>>>>>>>>>>>>>> sum(forwardBorndata) = ", sum(datatmp)
  if (verbose) write(0,*) ">>>>>>>>>>>>>>>> BORN ADJOINT <<<<<<<<<<<<<<<<<<<<<<<"
  call L_lop2(.true., .false., gnphi, datatmp, isou)
  ! if (verbose) write(0,*) ">>>>>>>>>>>>>>>> WEMVA FORWARD <<<<<<<<<<<<<<<<<<<<<<"
  ! call W_lop2(.true., .false., deltaB, modboth, isou)
  if (verbose) write(0,*) ">>>>>>>>>>>>>>>> sum(gnphi) = ", sum(gnphi)
  ! if (verbose) write(0,*) ">>>>>>>>>>>>>>>> sum(deltaB) = ", sum(deltaB)

  !-----------------------------------------------------------------------------
  if (verbose) write(0,*) ">>>>>>>>>>>>>>>> COMBINE COMPONENTS <<<<<<<<<<<<<<<<<"
  finalvel = gnphi! + deltaB
  if (verbose) write(0,*) ">>>>>>>>>>>>>>>> sum(finalvel) = ", sum(finalvel)

  !-----------------------------------------------------------------------------
  if (verbose) write(0,*) ">>>>>>>>>>>>>>>> Write output <<<<<<<<<<<<<<<<<<<<<<<"
  call sep_write(finalvel)

  !-----------------------------------------------------------------------------
  call DATE_AND_TIME(values=timer1)
  timer0 = timer1-timer0
  timer_sum = timer_sum + (((timer0(3)*24+timer0(5))*60+timer0(6))*60+timer0(7))*1000+timer0(8)
  write(0,*) "timer sum (sec)", timer_sum/1000
  write(0,*) "timer sum (min)", timer_sum/1000/60
  write(0,*) "DONE!"
  !-----------------------------------------------------------------------------

  if (exist_file('test')) then
    call to_history("n1", n1, 'test')
    call to_history("o1", o1, 'test')
    call to_history("d1", d1, 'test')
    call to_history("n2", n2, 'test')
    call to_history("o2", o2, 'test')
    call to_history("d2", d2, 'test')
    call sep_write(finalvel, 'test')
  endif

  if (exist_file('gncomp')) then
    call to_history("n1", n1, 'gncomp')
    call to_history("o1", o1, 'gncomp')
    call to_history("d1", d1, 'gncomp')
    call to_history("n2", n2, 'gncomp')
    call to_history("o2", o2, 'gncomp')
    call to_history("d2", d2, 'gncomp')
    call sep_write(gnphi, 'gncomp')
  endif

  ! if (exist_file('wemvacomp')) then
  !   call to_history("n1", n1, 'wemvacomp')
  !   call to_history("o1", o1, 'wemvacomp')
  !   call to_history("d1", d1, 'wemvacomp')
  !   call to_history("n2", n2, 'wemvacomp')
  !   call to_history("o2", o2, 'wemvacomp')
  !   call to_history("d2", d2, 'wemvacomp')
  !   call sep_write(deltaB, 'wemvacomp')
  ! endif

end program
