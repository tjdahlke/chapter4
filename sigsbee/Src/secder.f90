module secder_mod
  implicit none
  integer, private                    :: n1, n2, nt, it
  real, private                       :: idt2
  contains



  subroutine secder_init(n1_in, n2_in, nt_in, dt_in)
    integer                      :: n1_in, n2_in, nt_in
    real                         :: dt_in
    n1 = n1_in
    n2 = n2_in
    nt = nt_in
    idt2 = 1./dt_in/dt_in
  end subroutine



  function secder_op(adj, add, model, data) result(stat)
    logical,intent(in) :: adj, add
    real,dimension(:)  :: model, data
    integer            :: stat
    call secder_op2(adj, add, model, data)
    stat=0
  end function



  subroutine secder_op2(adj, add, model, data)
    logical,intent(in)          :: adj, add 
    real, dimension(n1,n2,nt) :: model, data
    if(adj) then
      if(.not. add) model = 0.
      model(:,:,1) = model(:,:,1) + (data(:,:,2)-data(:,:,1))*idt2
      model(:,:,2:nt-1) = model(:,:,2:nt-1) + (data(:,:,1:nt-2)-2.*data(:,:,2:nt-1)+data(:,:,3:nt))*idt2
      model(:,:,nt) = model(:,:,nt) + (data(:,:,nt-1)-data(:,:,nt))*idt2
    else
      if(.not. add) data = 0.
      data(:,:,1) = data(:,:,1) + (model(:,:,2)-model(:,:,1))*idt2
      data(:,:,2:nt-1) = data(:,:,2:nt-1) + (model(:,:,1:nt-2)-2.*model(:,:,2:nt-1)+model(:,:,3:nt))*idt2
      data(:,:,nt) = data(:,:,nt) + (model(:,:,nt-1)-model(:,:,nt))*idt2
    end if
  end subroutine



end module
