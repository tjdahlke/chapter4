
!===============================================================================
!		Applies the derivative with respect to the velocity model: M(phi)
!		MODEL:  NP	(perturbations in phi, sparsely represented)
!		DATA:   NZxNX	(velocity model perturbation)
!
!		(Just uses phi parameter, not b!)
!===============================================================================

PROGRAM APPLY_Dop_RBF1
  USE sep
  USE rbf_mod
  IMPLICIT NONE

  INTEGER														:: nrbf, n1, n2, np, n1t, trad
  REAL															:: o1,o2,d1,d2
  LOGICAL														:: verbose, adjoint, add
  REAL, DIMENSION(:,:), ALLOCATABLE	:: rbfcoord,rbftable,scaling,DATA,temp,masksalt
  REAL, DIMENSION(:), ALLOCATABLE		:: sparsemodel
  INTEGER, DIMENSION(8)							:: timer0, timer1
  REAL															:: timer_sum=0.

  CALL sep_init()
  CALL DATE_AND_TIME(values=timer0)

  IF(verbose) WRITE(0,*) "========= Read in initial parameters =================="
  CALL from_param("verbose",verbose,.FALSE.)
  CALL from_param("adjoint",adjoint,.FALSE.)
  CALL from_param("add",add,.FALSE.)
  CALL from_aux("scaling", "n1", n1)
  CALL from_aux("scaling", "n2", n2)
  CALL from_aux("scaling", "o2", o2)
  CALL from_aux("scaling", "o1", o1)
  CALL from_aux("scaling", "d1", d1)
  CALL from_aux("scaling", "d2", d2)
  CALL from_aux("rbfcoord", "n1", nrbf)
  CALL from_param("trad",trad,15)
  CALL from_history("n1",n1t)
  np=nrbf


  IF (adjoint) THEN
     IF  (n1t.NE.n1) THEN
        WRITE(0,*) ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>><<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<"
        WRITE(0,*) ">>>>>>>>>>>>>>>>>>>>>  Input file is not the right dimensions <<<<<<<<<<<<<<<<"
        STOP
     ENDIF
  ENDIF

  IF(verbose) WRITE(0,*) "========= Allocate ==================================="
  ALLOCATE(scaling(n1, n2))
  ALLOCATE(masksalt(n1, n2))
  ALLOCATE(DATA(n1, n2))
  ALLOCATE(sparsemodel(np))
  ALLOCATE(temp(n1, n2))
  ALLOCATE(rbfcoord(nrbf,2))
  ALLOCATE(rbftable((2*trad+1),(2*trad+1)))

  IF(verbose) WRITE(0,*) "========= Read in masking, scaling, RBF coords =================="
  CALL sep_read(scaling, "scaling")
  CALL sep_read(masksalt, "masksalt")
  CALL sep_read(rbfcoord,"rbfcoord")
  CALL sep_read(rbftable,"rbftable")


  IF(verbose) WRITE(0,*) "sum(masksalt) = ", SUM(masksalt)
  IF(verbose) WRITE(0,*) "sum(scaling) = ", SUM(scaling)

  IF(verbose) WRITE(0,*) "========= Initialize submodules ======================"
  CALL rbf_init(n1, n2, nrbf, trad, rbfcoord, rbftable)
  CALL rbf_init_mask(masksalt)


  IF(verbose) WRITE(0,*) "========= Write .H file parameters ==================="
  IF (adjoint) THEN
     CALL to_history("n1",np)
     CALL to_history("o1",0.)
     CALL to_history("d1",1.)
     CALL to_history("n2",1)
     CALL to_history("o2",0.)
     CALL to_history("d2",1.)
     CALL to_history("n3",1)
     CALL to_history("o3",0)
     CALL to_history("d3",1.0)
     CALL to_history("label1",'sparsemodel points')
  ELSE
     CALL to_history("n1",n1)
     CALL to_history("n2",n2)
     CALL to_history("o1",o1)
     CALL to_history("o2",o2)
     CALL to_history("d1",d1)
     CALL to_history("d2",d2)
     CALL to_history("n3",1)
     CALL to_history("o3",0)
     CALL to_history("d3",1.0)
     CALL to_history("label1",'z [km]')
     CALL to_history("label2",'x [km]')
  ENDIF

  IF (adjoint) THEN
     !	deltaM -----> deltaPhiRBF (sparse)
     CALL sep_read(DATA)
     IF (.NOT. add) sparsemodel=0.0
     temp = temp + DATA*scaling
     CALL L_Hrbf(.TRUE.,.FALSE.,sparsemodel,temp)
     CALL sep_write(sparsemodel)
  ELSE
     !	deltaPhiRBF (sparse) -----> deltaM
     CALL sep_read(sparsemodel)
     IF (.NOT. add) DATA=0.0
     CALL L_Hrbf(.FALSE.,.FALSE.,sparsemodel,temp)
     DATA = DATA + temp*scaling
     CALL sep_write(DATA)
  ENDIF

  !-----------------------------------------------------------------------------
  CALL DATE_AND_TIME(values=timer1)
  timer0 = timer1-timer0
  timer_sum = timer_sum + (((timer0(3)*24+timer0(5))*60+timer0(6))*60+timer0(7))*1000+timer0(8)
  WRITE(0,*) "timer sum (sec): ", timer_sum/1000

  ! if (exist_file('test')) then
  ! 	call to_history("n1"  ,n1,	'test')
  ! 	call to_history("n2"  ,n2,	'test')
  ! 	call sep_write(modelB ,		'test')
  ! end if

  IF (verbose) WRITE(0,*) "APPLY_Dop_RBF program complete"
  DEALLOCATE(sparsemodel,rbfcoord,rbftable,scaling,DATA,temp,masksalt)


END PROGRAM APPLY_Dop_RBF1
