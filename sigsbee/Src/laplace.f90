module laplace_mod

  use mem_mod

  implicit none

  integer, private                            :: n1, n2, fat, iz, ix
  real, private                               :: stencil0
  real, dimension(5), private                 :: stencilz, stencilx
  real, dimension(:,:), allocatable, private  :: temp

  contains

  subroutine laplace_init(n1_in, n2_in, d1_in, d2_in)
    integer                    :: n1_in, n2_in
    real                       :: d1_in, d2_in

    n1 = n1_in
    n2 = n2_in

    fat = 5

    if(.not. allocated(temp)) then
      allocate(temp(1-fat:n1+fat,1-fat:n2+fat))
      call mem_add(size(temp))
    end if

    stencilz(1)=3.333333333/(2.*d1_in*d1_in);
    stencilz(2)=-0.4761904762/(2.*d1_in*d1_in);
    stencilz(3)=0.0793650794/(2.*d1_in*d1_in);
    stencilz(4)=-0.0099206349/(2.*d1_in*d1_in);
    stencilz(5)=0.0006349206/(2.*d1_in*d1_in);

    stencilx(1)=3.333333333/(2.*d2_in*d2_in);
    stencilx(2)=-0.4761904762/(2.*d2_in*d2_in);
    stencilx(3)=0.0793650794/(2.*d2_in*d2_in);
    stencilx(4)=-0.0099206349/(2.*d2_in*d2_in);
    stencilx(5)=0.0006349206/(2.*d2_in*d2_in);

    stencil0=(stencilz(5)+stencilx(5))
    stencil0=stencil0+(stencilz(4)+stencilx(4))
    stencil0=stencil0+(stencilz(3)+stencilx(3))
    stencil0=stencil0+(stencilz(2)+stencilx(2))
    stencil0=stencil0+(stencilz(1)+stencilx(1))
    stencil0=-2.*stencil0
  end subroutine

  function laplace_op(adj, add, model, data) result(stat)
    logical,intent(in) :: adj, add
    real,dimension(:)  :: model, data
    integer            :: stat
    call laplace_op2(adj, add, model, data)
    stat=0
  end function

  subroutine laplace_op2(adj, add, model, data)
    logical,intent(in)     :: adj, add
    real, dimension(n1,n2) :: model, data
    if(adj) then
      if(.not. add) model = 0.
      temp(1:n1,1:n2) = data
      !$OMP DO
      do ix=1,n2
        do iz=1,n1
          model(iz,ix) = model(iz,ix) + stencil0*temp(iz,ix)+&
                        stencilz(1)*(temp(iz-1,ix  )+temp(iz+1,ix  ))+&
                        stencilx(1)*(temp(iz  ,ix-1)+temp(iz  ,ix+1))+&
                        stencilz(2)*(temp(iz-2,ix  )+temp(iz+2,ix  ))+&
                        stencilx(2)*(temp(iz  ,ix-2)+temp(iz  ,ix+2))+&
                        stencilz(3)*(temp(iz-3,ix  )+temp(iz+3,ix  ))+&
                        stencilx(3)*(temp(iz  ,ix-3)+temp(iz  ,ix+3))+&
                        stencilz(4)*(temp(iz-4,ix  )+temp(iz+4,ix  ))+&
                        stencilx(4)*(temp(iz  ,ix-4)+temp(iz  ,ix+4))+&
                        stencilz(5)*(temp(iz-5,ix  )+temp(iz+5,ix  ))+&
                        stencilx(5)*(temp(iz  ,ix-5)+temp(iz  ,ix+5))
        end do
      end do
      !$OMP END DO
    else
      if(.not. add) data = 0.
      temp(1:n1,1:n2) = model
      !$OMP DO
      do ix=1,n2
        do iz=1,n1
          data(iz,ix) = data(iz,ix) + stencil0*temp(iz,ix)+&
                        stencilz(1)*(temp(iz-1,ix  )+temp(iz+1,ix  ))+&
                        stencilx(1)*(temp(iz  ,ix-1)+temp(iz  ,ix+1))+&
                        stencilz(2)*(temp(iz-2,ix  )+temp(iz+2,ix  ))+&
                        stencilx(2)*(temp(iz  ,ix-2)+temp(iz  ,ix+2))+&
                        stencilz(3)*(temp(iz-3,ix  )+temp(iz+3,ix  ))+&
                        stencilx(3)*(temp(iz  ,ix-3)+temp(iz  ,ix+3))+&
                        stencilz(4)*(temp(iz-4,ix  )+temp(iz+4,ix  ))+&
                        stencilx(4)*(temp(iz  ,ix-4)+temp(iz  ,ix+4))+&
                        stencilz(5)*(temp(iz-5,ix  )+temp(iz+5,ix  ))+&
                        stencilx(5)*(temp(iz  ,ix-5)+temp(iz  ,ix+5))
        end do
      end do
      !$OMP END DO
    end if
  end subroutine

end module
