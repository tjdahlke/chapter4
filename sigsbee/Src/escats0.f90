module escats0_mod

  implicit none

  integer, private                         :: n1, n2, nt, nh, oh, it, ih, h, ix, iz
  real, dimension(:,:,:), pointer, private :: wflds0

  contains

  subroutine escats0_init(n1_in, n2_in, nt_in, nh_in, oh_in, wflds0_in)
    integer                        :: n1_in, n2_in, nt_in, nh_in, oh_in
    real, dimension(:,:,:), target :: wflds0_in
    n1 = n1_in
    n2 = n2_in
    nt = nt_in
    nh = nh_in
    oh = oh_in
    wflds0 => wflds0_in
  end subroutine

  function escats0_op(adj, add, model, data) result(stat)
    logical,intent(in) :: adj, add
    real,dimension(:)  :: model, data
    integer            :: stat
    call escats0_op2(adj, add, model, data)
    stat=0
  end function

  subroutine escats0_op2(adj, add, model, data)
    logical,intent(in)          :: adj, add 
    real, dimension(n1,n2,nh)   :: model
    real, dimension(n1,n2,nt) :: data
    if(adj) then
      if(.not. add) model = 0.
      do it=1,nt
        do ih=1,nh
          h = ih - 1 + oh
          do ix=1+abs(h),n2-abs(h)
            model(:,ix,ih) = model(:,ix,ih) + data(:,ix+h,it)*wflds0(:,ix-h,it)
          end do
        end do
      end do
    else
      if(.not. add) data = 0.
      do it=1,nt
        do ih=1,nh
          h = ih - 1 + oh
          do ix=1+abs(h)+h,n2-abs(h)+h
            data(:,ix,it) = data(:,ix,it) + model(:,ix-h,ih)*wflds0(:,ix-2*h,it)
          end do
        end do
      end do
    end if
  end subroutine

end module
