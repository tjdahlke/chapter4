

#include "boost/multi_array.hpp"
#include <tbb/tbb.h>
#include <tbb/blocked_range.h>
#include "timer.h"
#include "laplacian2d.h"
#include "bcder2d.h"
#include <float3DReg.h>
#include <float2DReg.h>
#include <float1DReg.h>
#include <ioModes.h>
using namespace SEP;
using namespace giee;
using namespace std;

typedef boost::multi_array<float, 2> float2D;

class block {
public:
block(int b1,int e1,int b2,int e2) : _b1(b1),_e1(e1),_b2(b2),_e2(e2){
};
int _b1,_e1,_b2,_e2;
};



int main(int argc, char **argv){

        timer x2=timer("FULL PROGRAM",1);
        x2.start();

        // Setup the IO
        ioModes modes(argc,argv);
        std::shared_ptr<genericIO> io=modes.getDefaultIO();
        std::shared_ptr<paramObj> par=io->getParamObj();

        // Read the input IO streams
        std::shared_ptr<genericRegFile> inp=io->getRegFile("in",usageIn);
        std::shared_ptr<genericRegFile> sou=io->getRegFile("sou",usageIn);
        std::shared_ptr<genericRegFile> rec=io->getRegFile("rec",usageIn);
        std::shared_ptr<genericRegFile> wav=io->getRegFile("wavelet",usageIn);
        std::shared_ptr<hypercube> hyperVel=inp->getHyper();
        std::shared_ptr<hypercube> hyperSou=sou->getHyper();
        std::shared_ptr<hypercube> hyperRec=rec->getHyper();
        std::shared_ptr<hypercube> hyperWav=wav->getHyper();

        // Setup the wavelet input
        std::shared_ptr<float1DReg> wavelet(new float1DReg(hyperWav));
        wav->readFloatStream(wavelet->getVals(),hyperWav->getN123());

        // Setup the velocity model input
        fprintf(stderr,">>>>> Reading in the VEL MODEL \n");
        std::shared_ptr<float2DReg> velModel(new float2DReg(hyperVel));
        inp->readFloatStream(velModel->getVals(),hyperVel->getN123());

        // Setup the sou position coordinates input
        std::shared_ptr<float2DReg> soucoor(new float2DReg(hyperSou));
        sou->readFloatStream(soucoor->getVals(),hyperSou->getN123());

        // Setup the reciever position coordinates input
        std::shared_ptr<float2DReg> reccoor(new float2DReg(hyperRec));
        rec->readFloatStream(reccoor->getVals(),hyperRec->getN123());

        // Get modelspace parameters
        std::vector<int> velN=hyperVel->getNs();
        long long nz=velN[0];
        long long nx=velN[1];
        // long long n1=nz,n2=nx;
        float dz = velModel->getHyper()->getAxis(1).d;
        float dx = velModel->getHyper()->getAxis(2).d;

        // Get acquisition parameters
        std::vector<int> souN=hyperSou->getNs();
        std::vector<int> recN=hyperRec->getNs();
        int Nsou=souN[0];
        int Nrec=recN[0];

        // Get ABC parameters
        int bc1=par->getInt("bc1",100);
        int bc2=par->getInt("bc2",100);
        int bc3=par->getInt("bc3",100);
        int bc4=par->getInt("bc4",100);
        int alpha=par->getFloat("alpha",0.05);

        // Get propagation parameters
        int nt=par->getInt("nt",100);
        float vconst=par->getFloat("vconst",2.0);
        float dsamp=par->getFloat("dsamp",0.1);
        float dt=par->getFloat("dt",0.002);
        float sc=dt*dt/dsamp/dsamp;

        // Set blocksize
        int bs=par->getInt("bs",20);
        int bs1,bs2;
        bs1=bs;
        bs2=bs;
        fprintf(stderr,">>>>> Done reading inputs \n");


        // WAVEFIELD OUTPUT
        SEP::axis axZ(nz,0.0,dsamp);
        SEP::axis axX(nx,0.0,dsamp);
        SEP::axis axT(nt,0.0,dt);
        std::shared_ptr<hypercube> hyperOut3d(new hypercube(axZ,axX,axT));
        std::shared_ptr<float3DReg> wave(new float3DReg(hyperOut3d));
        std::shared_ptr<genericRegFile> outWV=io->getRegFile("wave",usageOut);
        outWV->setHyper(hyperOut3d);
        outWV->writeDescription();
        fprintf(stderr,">>>>> Done setting up the wavefield output \n");

        // SHOT GATHER OUTPUT
        SEP::axis axR(Nrec,0.0,1.0);
        std::shared_ptr<hypercube> hyperOut2d(new hypercube(axT,axR));
        std::shared_ptr<float2DReg> gather(new float2DReg(hyperOut2d));
        std::shared_ptr<genericRegFile> outSG=io->getRegFile("out",usageOut);
        outSG->setHyper(hyperOut2d);
        outSG->writeDescription();
        fprintf(stderr,">>>>> Done setting up the shotgather output \n");


        // ABC DAMPENING OUTPUT
        std::shared_ptr<hypercube> hyperOut2dABC(new hypercube(axZ,axX));
        std::shared_ptr<float2DReg> abc(new float2DReg(hyperOut2dABC));
        std::shared_ptr<genericRegFile> outABC=io->getRegFile("abcs",usageOut);
        outABC->setHyper(hyperOut2dABC);
        outABC->writeDescription();
        fprintf(stderr,">>>>> Done setting up the ABC dampening output \n");


        // Copy the velocity model
        std::shared_ptr<float2D> vsq(new float2D(boost::extents[nx][nz]));
        for(int ix=0; ix < nx; ix++) {
                for(int iz=0; iz < nz; iz++) {
                        long long ii = ix*nz + iz;
                        vsq->data()[ii] = (*velModel->_mat)[ix][iz];
                }
        }
        fprintf(stderr,">>>>> Done making exponential boundary condition weights \n");



        // =====================================================================
        
        float coef[6];
        coef[1]=.83333333*sc;
        coef[2]=-.119047619*sc;
        coef[3]=.0198412698*sc;
        coef[4]=.00248015873*sc;
        coef[5]=.0001587301587*sc;
        coef[0]=-4*(coef[1]+coef[2]+coef[3]+coef[4]+coef[5]);

        std::shared_ptr<float2D> oldW(new float2D(boost::extents[nx][nz]));
        std::shared_ptr<float2D> curW(new float2D(boost::extents[nx][nz]));
        std::shared_ptr<float2D> newW(new float2D(boost::extents[nx][nz]));
        std::shared_ptr<float2D> newW2(new float2D(boost::extents[nx][nz]));
        std::shared_ptr<float2D> tmp(new float2D(boost::extents[nx][nz]));


        // Initialize zero arrays for pressure fields
        for(long long i=0; i < nz*nx; i++) {
                oldW->data()[i]=0;
                curW->data()[i]=0;
        }

        timer x=timer("BIG LOOP",nz*nx/1000);


        // =====================================================================
        vector<int> b1(1,5),e1(1,5+bs1);
        int nleft=nz-10-bs1,i=0;
        while(nleft>0) {
                int bs=min(nleft,bs1);
                b1.push_back(e1[i]);
                e1.push_back(e1[i]+bs);
                ++i;
                nleft-=bs;
        }

        vector<int> b2(1,5),e2(1,5+bs2);
        nleft=nx-10-bs2;
        i=0;
        while(nleft>0) {
                int bs=min(nleft,bs2);
                b2.push_back(e2[i]);
                e2.push_back(e2[i]+bs);
                ++i;
                nleft-=bs;
        }

        vector<block> blocks;
        for(int i2=0; i2<b2.size(); ++i2) {
                for(int i1=0; i1<b1.size(); ++i1) {
                        blocks.push_back(block(b1[i1],e1[i1],b2[i2],e2[i2]));
                }
        }

        // Specify source position
        int isou=0;
        int souz=(*soucoor->_mat)[0][isou];
        int soux=(*soucoor->_mat)[1][isou];
        fprintf(stderr,"souz = %d\n",souz);
        fprintf(stderr,"soux = %d\n",soux);


        //========  MAKE ABC WEIGHTS ========================================

        // Initialize array of ones
        for(long long iz=0; iz < nz; iz++){
                for(long long ix=0; ix < nx; ix++){
                        (*abc->_mat)[iz][ix]=1.0;
                }
        }

        // Assign the exponential weights to the ABC dampening
        float pi=3.14159265358979323846;
        for(int ix=0; ix < nx; ix++) {
                for(int iz=0; iz < nz; iz++) {
                        if (ix <= bc3){
                                float xx = (ix-bc3)/(alpha*float(bc3));
                                // (*abc->_mat)[ix][iz] = (*abc->_mat)[ix][iz]*cos(xx*pi/2);
                                (*abc->_mat)[ix][iz] = (*abc->_mat)[ix][iz]*exp(-1.0*alpha*pow(xx,2));
                        }
                        else if ((nx-bc4) <= ix){
                                float xx = (ix-nx+bc3)/(alpha*float(bc3));
                                // (*abc->_mat)[ix][iz] = (*abc->_mat)[ix][iz]*cos(xx*pi/2);
                                (*abc->_mat)[ix][iz] = (*abc->_mat)[ix][iz]*exp(-1.0*alpha*pow(xx,2));
                        }
                        if (iz <= bc1){
                                float zz = (iz-bc1)/(alpha*float(bc1));
                                // (*abc->_mat)[ix][iz] = (*abc->_mat)[ix][iz]*cos(zz*pi/2);
                                (*abc->_mat)[ix][iz] = (*abc->_mat)[ix][iz]*exp(-1.0*alpha*pow(zz,2));
                        }
                        else if ((nz-bc2) <= iz){
                                float zz = (iz-nz+bc1)/(alpha*float(bc1));
                                // (*abc->_mat)[ix][iz] = (*abc->_mat)[ix][iz]*cos(zz*pi/2);
                                (*abc->_mat)[ix][iz] = (*abc->_mat)[ix][iz]*exp(-1.0*alpha*pow(zz,2));
                        }
                }
        }

        // Copy the ABC weights
        std::shared_ptr<float2D> abcW(new float2D(boost::extents[nx][nz]));
        for(int ix=0; ix < nx; ix++) {
                for(int iz=0; iz < nz; iz++) {
                        long long ii = ix*nz + iz;
                        abcW->data()[ii] = (*abc->_mat)[ix][iz];
                }
        }




        //========  MAIN WAVEFIELD LOOP ========================================
        for(int it=0; it < nt; it++) {
                fprintf(stderr,"Percent done: %f \n",100*float(it)/float(nt));
                x.start();

                tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                        for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                        int i=blocks[ib]._b1+i2*nz;
                                        laplacian2d(blocks[ib]._e1-blocks[ib]._b1,nz,coef,vsq->data()+i,abcW->data()+i,oldW->data()+i,curW->data()+i,newW->data()+i);
                                }
                        }
                });


                newW2=newW;

                // // Ali style ABC's
                // for(int ix=0; ix < nx; ix++){
                //         // for(int iz=0; iz < bc1; iz++){
                //         //         (*newW2)[ix][iz] = (*newW)[ix][iz] - (*newW)[ix][iz+1];
                //         // }
                //         for(int iz=nz-bc3; iz < nz; iz++){
                //                 (*newW2)[ix][iz] = (*newW)[ix][iz] - (*newW)[ix][iz-1];
                //         }
                // }
                // for(int ix=0; ix < bc2; ix++){
                //         for(int iz=0; iz < nz; iz++){
                //                 (*newW2)[ix][iz] = (*newW)[ix][iz] - (*newW)[ix+1][iz];
                //         }
                // }
                // for(int ix=nx-bc4; ix < nx; ix++){
                //         for(int iz=0; iz < nz; iz++){
                //                 (*newW2)[ix][iz] = (*newW)[ix][iz] - (*newW)[ix-1][iz];
                //         }
                // }


                // // Extract the reciever recordings
                // for(int irec=0; irec < Nrec; irec++) {
                //         int recz=(*reccoor->_mat)[0][irec];
                //         int recx=(*reccoor->_mat)[1][irec];
                //         (*gather->_mat)[irec][it]=(*curW)[recx][recz];
                // }


                // Extract the wavefield
                (*wave->_mat)[it] = (*curW);

                // Update source
                (*newW2)[soux][souz]+=(*wavelet->_mat)[it];

                // Update the wavefields
                // tmp=oldW; oldW=curW; curW=newW; newW=tmp;
                tmp=oldW; oldW=curW; curW=newW2; newW2=tmp;

                newW = newW2;

                x.stop();
        }
        // =====================================================================

        x.print();

        float m=newW2->data()[0],mm=newW2->data()[0];
        for(long long i=1; i < nz*nx; i++) {
                if(newW2->data()[i]>mm) mm=newW2->data()[i];
                if(newW2->data()[i]<mm) m=newW2->data()[i];
        }
        fprintf(stderr,"min %f max %f\n",m,mm);


        // =============================================================================
        // Write the outputs to SEP files
        outABC->writeFloatStream(abc->getVals(),hyperOut2dABC->getN123());  // ABC weights
        outWV->writeFloatStream(wave->getVals(),hyperOut3d->getN123());  // Wavefield
        // outSG->writeFloatStream(gather->getVals(),hyperOut2d->getN123());  // Shot gather
        x2.stop();
        x2.print();

}
