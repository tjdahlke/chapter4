module bcder_mod

  implicit none

  integer, private                         :: n1, n2, ix, iz
  integer, private                         :: bc1, bc2, bc3, bc4

  contains

  subroutine bcder_init(n1_in, n2_in, bc1_in, bc2_in, bc3_in, bc4_in)
    integer                        :: n1_in, n2_in, bc1_in, bc2_in, bc3_in, bc4_in
    n1 = n1_in
    n2 = n2_in
    bc1 = bc1_in
    bc2 = bc2_in
    bc3 = bc3_in
    bc4 = bc4_in
  end subroutine

  function bcder_op(adj, add, model, data) result(stat)
    logical,intent(in) :: adj, add
    real,dimension(:)  :: model, data
    integer            :: stat
    call bcder_op2(adj, add, model, data)
    stat=0
  end function

  subroutine bcder_op2(adj, add, model, data)
    logical,intent(in)     :: adj, add 
    real, dimension(n1,n2) :: model
    real, dimension(n1,n2) :: data
    if(adj) then
      if(.not. add) model = 0.
      model(1,:) = model(1,:) + data(1,:)
      do iz=2,bc1
        model(iz,:) = model(iz,:) + data(iz,:) - data(iz-1,:)
      end do
      model(bc1+1,:) = model(bc1+1,:) - data(bc1,:)
      model(n1-bc3,:) = model(n1-bc3,:) - data(n1-bc3+1,:)
      do iz=n1-bc3+1,n1-1
        model(iz,:) = model(iz,:) + data(iz,:) - data(iz+1,:)
      end do
      model(n1,:) = model(n1,:) + data(n1,:)
      model(:,1) = model(:,1) + data(:,1)
      do ix=2,bc2
        model(:,ix) = model(:,ix) + data(:,ix) - data(:,ix-1)
      end do
      model(:,bc1+1) = model(:,bc1+1) - data(:,bc1)
      model(:,n2-bc4) = model(:,n2-bc4) - data(:,n2-bc4+1)
      do ix=n2-bc4+1,n2-1
        model(:,ix) = model(:,ix) + data(:,ix) - data(:,ix+1)
      end do
      model(:,n2) = model(:,n2) + data(:,n2)
    else
      if(.not. add) data = 0.
      do iz=1,bc1
        data(iz,:) = data(iz,:) + model(iz,:) - model(iz+1,:)
      end do
      do iz=n1-bc3+1,n1
        data(iz,:) = data(iz,:) + model(iz,:) - model(iz-1,:)
      end do
      do ix=1,bc2
        data(:,ix) = data(:,ix) + model(:,ix) - model(:,ix+1)
      end do
      do ix=n2-bc4+1,n2
        data(:,ix) = data(:,ix) + model(:,ix) - model(:,ix-1)
      end do
    end if
  end subroutine

end module
