

#include "boost/multi_array.hpp"
#include <tbb/tbb.h>
#include <tbb/blocked_range.h>
#include "timer.h"
#include "laplacian3d.h"
#include <float3DReg.h>
#include <float2DReg.h>
#include <float1DReg.h>
#include <ioModes.h>
using namespace SEP;
using namespace giee;
using namespace std;

typedef boost::multi_array<float, 3> float3D;

class block {
public:
block(int b1,int e1,int b2,int e2,int b3,int e3) : _b1(b1),_e1(e1),_b2(b2),_e2(e2),_b3(b3),_e3(e3){
};
int _b1,_e1,_b2,_e2,_b3,_e3;
};



int main(int argc, char **argv){

        timer x=timer("FULL RUN");
        x.start();

        // Setup the IO
        ioModes modes(argc,argv);
        std::shared_ptr<genericIO> io=modes.getDefaultIO();
        std::shared_ptr<paramObj> par=io->getParamObj();

        // Read the input IO streams
        std::shared_ptr<genericRegFile> inp=io->getRegFile("in",usageIn);
        std::shared_ptr<genericRegFile> sou=io->getRegFile("sou",usageIn);
        std::shared_ptr<genericRegFile> rec=io->getRegFile("rec",usageIn);
        std::shared_ptr<genericRegFile> res=io->getRegFile("residual",usageIn);
        std::shared_ptr<genericRegFile> abc=io->getRegFile("abcs",usageIn);
        std::shared_ptr<hypercube> hyperVel=inp->getHyper();
        std::shared_ptr<hypercube> hyperSou=sou->getHyper();
        std::shared_ptr<hypercube> hyperRec=rec->getHyper();
        std::shared_ptr<hypercube> hyperAbc=abc->getHyper();
        std::shared_ptr<hypercube> hyperRes=res->getHyper();

        // Setup the residual recording input
        std::shared_ptr<float2DReg> residual(new float2DReg(hyperRes));
        res->readFloatStream(residual->getVals(),hyperRes->getN123());

        // Setup the velocity model input
        std::shared_ptr<float3DReg> velModel(new float3DReg(hyperVel));
        inp->readFloatStream(velModel->getVals(),hyperVel->getN123());

        // Setup the sou position coordinates input
        std::shared_ptr<float2DReg> soucoor(new float2DReg(hyperSou));
        sou->readFloatStream(soucoor->getVals(),hyperSou->getN123());

        // Setup the reciever position coordinates input
        std::shared_ptr<float2DReg> reccoor(new float2DReg(hyperRec));
        rec->readFloatStream(reccoor->getVals(),hyperRec->getN123());

        // Get modelspace parameters
        std::vector<int> velN=hyperVel->getNs();
        long long nz=velN[0];
        long long nx=velN[1];
        long long ny=velN[2];
        float dz = hyperVel->getAxis(1).d;
        float dx = hyperVel->getAxis(2).d;
        float dy = hyperVel->getAxis(3).d;
        float oz = hyperVel->getAxis(1).o;
        float ox = hyperVel->getAxis(2).o;
        float oy = hyperVel->getAxis(3).o;

        // Setup the absorbing boundary condition weights input
        std::shared_ptr<float3DReg> abcW(new float3DReg(hyperAbc));
        abc->readFloatStream(abcW->getVals(),hyperAbc->getN123());
        std::shared_ptr<float3D> abcW2(new float3D(boost::extents[ny][nx][nz]));
        for(int iy=0; iy < ny; iy++) {
                for(int ix=0; ix < nx; ix++) {
                        for(int iz=0; iz < nz; iz++) {
                                long long ii = iy*nx*nz + ix*nz + iz;
                                abcW2->data()[ii] = (*abcW->_mat)[iy][ix][iz];
                        }
                }
        }

        // Get acquisition parameters
        std::vector<int> souN=hyperSou->getNs();
        std::vector<int> recN=hyperRec->getNs();
        int Nsou=souN[0];
        int Nrec=recN[0];

        // Get propagation parameters
        int nt=par->getInt("nt",100);
        float vconst=par->getFloat("vconst",2.0);
        float dsamp=par->getFloat("dsamp",0.1);
        float dt=par->getFloat("dt",0.002);
        float sc=dt*dt/dsamp/dsamp;

        // Set blocksize
        int bs=par->getInt("bs",20);
        int bs1,bs2,bs3;
        bs1=bs;
        bs2=bs;
        bs3=bs;

        // Read in the velocity model
        std::shared_ptr<float3D> vsq(new float3D(boost::extents[ny][nx][nz]));
        for(int iy=0; iy < ny; iy++) {
                for(int ix=0; ix < nx; ix++) {
                        for(int iz=0; iz < nz; iz++) {
                                long long ii = iy*nx*nz + ix*nz + iz;
                                vsq->data()[ii] = (*velModel->_mat)[iy][ix][iz];
                        }
                }
        }
        fprintf(stderr,">>>>> Done reading inputs \n");

        // BORN IMAGE OUTPUT
        SEP::axis axZ(nz,oz,dz);
        SEP::axis axX(nx,ox,dx);
        SEP::axis axY(ny,oy,dy);
        std::shared_ptr<hypercube> hyperOutImg(new hypercube(axZ,axX,axY));
        std::shared_ptr<float3DReg> image(new float3DReg(hyperOutImg));
        std::shared_ptr<genericRegFile> outI=io->getRegFile("out",usageOut);
        outI->setHyper(hyperOutImg);
        outI->writeDescription();
        fprintf(stderr,">>>>> Done setting up the wavefield output \n");

        // WAVEFIELD OUTPUT
        SEP::axis axT(nt,0.0,dt);
        std::shared_ptr<hypercube> hyperOutWav(new hypercube(axZ,axX,axT));
        std::shared_ptr<float3DReg> wave(new float3DReg(hyperOutWav));
        std::shared_ptr<genericRegFile> outWV=io->getRegFile("wave",usageOut);
        outWV->setHyper(hyperOutWav);
        outWV->writeDescription();
        fprintf(stderr,">>>>> Done setting up the wavefield output \n");


        // =====================================================================
        // Initialize laplacian FD coefficients and pressure fields
        float coef[6];
        coef[1]=.83333333*sc;
        coef[2]=-.119047619*sc;
        coef[3]=.0198412698*sc;
        coef[4]=.00248015873*sc;
        coef[5]=.0001587301587*sc;
        coef[0]=-6*(coef[1]+coef[2]+coef[3]+coef[4]+coef[5]);

        std::shared_ptr<float3D> oldW(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> curW(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> newW(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> newW2(new float3D(boost::extents[ny][nx][nz]));
        std::shared_ptr<float3D> tmp(new float3D(boost::extents[ny][nx][nz]));

        for(long long i=0; i < nz*nx*ny; i++) {
                oldW->data()[i]=0;
                curW->data()[i]=0;
        }

        // =====================================================================
        vector<int> b1(1,5),e1(1,5+bs1);
        int nleft=nz-10-bs1,i=0;
        while(nleft>0) {
                int bs=min(nleft,bs1);
                b1.push_back(e1[i]);
                e1.push_back(e1[i]+bs);
                ++i;
                nleft-=bs;
        }

        vector<int> b2(1,5),e2(1,5+bs2);
        nleft=nx-10-bs2;
        i=0;
        while(nleft>0) {
                int bs=min(nleft,bs2);
                b2.push_back(e2[i]);
                e2.push_back(e2[i]+bs);
                ++i;
                nleft-=bs;
        }

        vector<int> b3(1,5),e3(1,5+bs3);
        nleft=ny-10-bs3;
        i=0;
        while(nleft>0) {
                int bs=min(nleft,bs3);
                b3.push_back(e3[i]);
                e3.push_back(e3[i]+bs);
                ++i;
                nleft-=bs;
        }

        vector<block> blocks;
        for(int i3=0; i3<b3.size(); ++i3) {
                for(int i2=0; i2<b2.size(); ++i2) {
                        for(int i1=0; i1<b1.size(); ++i1) {
                                blocks.push_back(block(b1[i1],e1[i1],b2[i2],e2[i2],b3[i3],e3[i3]));
                        }
                }
        }

        // Specify source position
        int isou=0;
        int souz=(*soucoor->_mat)[0][isou];
        int soux=(*soucoor->_mat)[1][isou];
        int souy=(*soucoor->_mat)[2][isou];
        fprintf(stderr,"souz = %d\n",souz);
        fprintf(stderr,"souy = %d\n",souy);
        fprintf(stderr,"soux = %d\n",soux);


        //========  MAIN WAVEFIELD LOOP ========================================
        for(int it=0; it < nt; it++) {
                fprintf(stderr,"Percent done: %f \n",100*float(it)/float(nt));

                tbb::parallel_for(tbb::blocked_range<int>(0,(int)blocks.size()),[&](const tbb::blocked_range<int>&r){
                        for(int ib=r.begin(); ib!=r.end(); ++ib) {
                                for(int i3=blocks[ib]._b3; i3 < blocks[ib]._e3; i3++) {
                                        for(int i2=blocks[ib]._b2; i2 < blocks[ib]._e2; i2++) {
                                                int i=blocks[ib]._b1+i2*nz+i3*nz*nx;
                                                laplacian3d(blocks[ib]._e1-blocks[ib]._b1,nz,nz*nx,coef,vsq->data()+i,abcW2->data()+i,oldW->data()+i,curW->data()+i,newW->data()+i);
                                        }
                                }
                        }
                });

                // Inject the receiver recordings
                for(int irec=0; irec < Nrec; irec++) {
                        int recz=(*reccoor->_mat)[0][irec];
                        int recx=(*reccoor->_mat)[1][irec];
                        int recy=(*reccoor->_mat)[2][irec];
                        (*newW)[recy][recx][recz] = (*residual->_mat)[irec][nt-1-it];
                }

                // Extract the wavefield
                (*wave->_mat)[it] = (*curW)[souy];

                // Update the wavefields
                tmp=oldW; oldW=curW; curW=newW; newW=tmp;

        }
        // =====================================================================

        float m=newW->data()[0],mm=newW->data()[0];
        for(long long i=1; i < nz*nx*ny; i++) {
                if(newW->data()[i]>mm) mm=newW->data()[i];
                if(newW->data()[i]<mm) m=newW->data()[i];
        }
        fprintf(stderr,"min %f max %f\n",m,mm);


        // =============================================================================
        // Write the outputs to SEP files
        outI->writeFloatStream(image->getVals(),hyperOutImg->getN123());  // Shot gather
        outWV->writeFloatStream(wave->getVals(),hyperOutWav->getN123());  // Wavefield

        x.stop();
        x.print();

}
