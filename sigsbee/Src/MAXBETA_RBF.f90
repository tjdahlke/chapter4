program MAXBETA_RBF
use sep
use mem_mod
implicit none

real                               		:: upperlim, maxbetaval
integer									:: nz, nx
logical									:: verbose
real, dimension(:,:), allocatable		:: phi_grad, phi

call sep_init()
call from_param("n1", nz)
call from_param("n2", nx)
call from_param("upperlim", upperlim)
call from_param("verbose", verbose,.false.)

!---------------- Allocate ------------------------------
if (verbose) write(0,*) " MAXBETA: Allocating"
allocate(phi(nz, nx))
allocate(phi_grad(nz, nx))

!------------ Read in values ------------------------------
if (verbose) write(0,*) " MAXBETA: Reading in arrays"
call sep_read(phi, "phi")
call sep_read(phi_grad, "phigrad")
maxbetaval=upperlim*MAXVAL(ABS(phi))/(MAXVAL(ABS(phi_grad)))
if (verbose) write(0,*) "maxbetaval = ", maxbetaval

! !(((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((((
!-----------  Write outputs ------------------------------------------------
OPEN(UNIT=1,FILE="maxbetaval.txt",FORM="FORMATTED",STATUS="REPLACE",ACTION="WRITE")
WRITE(UNIT=1, FMT=*) maxbetaval
CLOSE(UNIT=1)

end program
