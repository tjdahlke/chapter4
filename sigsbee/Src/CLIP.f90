
!===============================================================================
!		Applies the clipping
!===============================================================================

program CLIP
use sep
implicit none

	integer										:: n1, n2, n3
	logical										:: verbose, lessthan
	real										:: clipval,replaceval
	real, dimension(:,:), allocatable			:: input2d
	real, dimension(:,:,:), allocatable			:: input3d

	call sep_init()
	if(verbose) write(0,*) "========= Read in initial parameters =================="
	call from_param("verbose",verbose,.false.)
	call from_param("clipval",clipval,1.5)
	call from_param("lessthan",lessthan,.true.)
	call from_param("replaceval",replaceval,clipval)

	call from_history("n1",n1)
	call from_history("n2",n2)
	call from_history("n3",n3,1)

	if(verbose) write(0,*) "replaceval = ", replaceval
	if(verbose) write(0,*) "clipval = ", clipval
	if(verbose) write(0,*) "lessthan = ", lessthan
	if(verbose) write(0,*) "maxval(input2d) = ", maxval(input2d)


	if (n3==1) then
		if(verbose) write(0,*) "========= Detected 2D input ==============="
		allocate(input2d(n1, n2))
		call sep_read(input2d)
		if (lessthan) then
			WHERE (input2d <=clipval) input2d = replaceval
		else
			WHERE (input2d >clipval) input2d = replaceval
		end if
		if(verbose) write(0,*) "maxval(input2d) = ", maxval(input2d)
		call sep_write(input2d)
	else
		if(verbose) write(0,*) "========= Detected 3D input ==============="
		allocate(input3d(n1, n2, n3))
		call sep_read(input3d)
		if (lessthan) then
			WHERE (input3d <=clipval) input3d = replaceval
		else
			WHERE (input3d >clipval) input3d = replaceval
		end if
		if(verbose) write(0,*) "maxval(input3d) = ", maxval(input3d)
		call sep_write(input3d)
	end if

end program
