
extern "C" {
void first_deriv2oZ( int n, int n1, float *input, float *output, float h, float scale);
}

extern "C" {
void first_deriv2oY(int n, int n1, float *input, float *output, float h, float scale);
}

extern "C" {
void laplacianFOR2d(int n, int n1, float *coef,float *v2dt,float *prev,float *cur,float *next,float *nextF);
}

extern "C" {
void scatFOR2d(int n, float *wvfld,float *data, float *refl);
}

extern "C" {
void addABC( int n, float dt, float *abcW, float *vel, float *cur, float *curD, float *newW, float *newABC);
}

extern "C" {
void sumABCs( int n,  float *curZ,  float *curY, float *output);
}

extern "C" {
void simple_mult( int n,  float *input,  float *output, float h);
}

extern "C" {
void waveADJ2d(int n, int n1, float *coef,float *prev,float *curT,float *cur,float *next,float *nextF);
}

extern "C" {
void scatADJ2d(int n, float *wvfld,float *data, float *refl);
}

extern "C" {
void scatADJ2dext(int n, int n1, int n12, int ylag, int iy, float *wvfld,float *data, float *refl);
}

extern "C" {
void comboBornFOR2d(int n, float *v2dt, float *model1, float *prev,float *curr, float *next, float *out, float dt2);
}

extern "C" {
void comboBornADJ2d(int n, float *v2dt, float *prev,float *curr, float *next, float *out, float dt2);
}










extern "C" {
void laplacianADJ2d(int n, int n1, float *coef,float *v2dt,float *prev,float *cur,float *next,float *nextF);
}

extern "C" {
void scale2d(int n, float *model,float *data, float *coef);
}

extern "C" {
void secder2d(int n, float *prev,float *curr, float *next, float *out, float dt2);
}
















