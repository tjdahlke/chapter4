
//======================================================================
//  Program to convert 3 coordinate text files to .H (SEP) files
//  input: text file with lines in format:  float1 float2 float3
//  output: SEP .H file (2D) nlines x 3
//======================================================================
#include <fstream>
#include <string>
#include <float2DReg.h>
#include <ioModes.h>
using namespace std;
using namespace SEP;
using namespace giee;


int main(int argc, char **argv){

        // Setup the IO
        ioModes modes(argc,argv);
        std::shared_ptr<genericIO> io=modes.getDefaultIO();

        // Count the number of lines (shot or rec positions)
        std::ifstream file(argv[1]);
        int npos = std::count(std::istreambuf_iterator<char>(file),
                              std::istreambuf_iterator<char>(), '\n');
        fprintf(stderr, "npos = %d\n", npos);

        // Make output hypercube
        SEP::axis ax1(npos,0.0,1.0);
        SEP::axis ax2(4,0.0,1.0); // number, depth, x, y
        std::shared_ptr<hypercube> hyperOut(new hypercube(ax1,ax2));

        // Set up the output IO stream
        std::shared_ptr<genericRegFile> outp=io->getRegFile("out",usageOut);
        outp->setHyper(hyperOut);
        outp->writeDescription();

        // Setup the output array
        std::shared_ptr<float2DReg> outC(new float2DReg(hyperOut));
        fprintf(stderr,">>>>> Done setting up the output stream \n");

        // Read contents of file and turn into coordinate .H file
        std::ifstream file1(argv[1]);
        std::string line;
        vector<string> tokens;
        int il=0;
        fprintf(stderr,">>>>> Done reading contents of file \n");

        // Put the text file values into an array
        while (std::getline(file1,line))
        {
                tokens.clear();
                istringstream iss(line);
                copy(istream_iterator<string>(iss),
                     istream_iterator<string>(),
                     back_inserter(tokens));
                (*outC->_mat)[0][il] = stof(tokens[0]);
                (*outC->_mat)[1][il] = stof(tokens[1]);
                (*outC->_mat)[2][il] = stof(tokens[2]);
                (*outC->_mat)[3][il] = stof(tokens[3]);
                il++;
        }

        // Write the array out as a SEP . H file
        outp->writeFloatStream(outC->getVals(),hyperOut->getN123());
        fprintf(stderr,">>>>> Done with program \n");

}
