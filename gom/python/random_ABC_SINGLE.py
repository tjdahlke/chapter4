#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#
#   PROGRAM MAKES RANDOM ABCS FOR EACH NODE
#
#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


#!/usr/local/bin/python
import sys
import os
import math
import commands
import sepbase
import sep2npy
import numpy as np
import subprocess


# Read the inputs
if len(sys.argv) < 2:
    print('Error: Not enough input args')
    sys.exit()
nodeId = int(sys.argv[1])
pad = sys.argv[2]
binpath = sys.argv[3]
genpar = sys.argv[4]
inputvel = sys.argv[5]
outputFileName = sys.argv[6]
writepath = sys.argv[7]
randpadin = int(sys.argv[8])
scale = int(sys.argv[9])
coarsevelIN="%s/coarsevelIN%d.h" % (writepath,nodeId)
coarsevelOUT="%s/coarsevelOUT%d.h" % (writepath,nodeId)
randpad=int(float(randpadin)/float(scale))

# Pad the model
cmd0="Pad extend=0 beg1=%d end1=%d beg2=%d end2=%d beg3=%d end3=%d < %s > %s/temp%d.h0" % (randpadin,randpadin,randpadin,randpadin,randpadin,randpadin,inputvel,writepath,nodeId)
cmd1="Pad extend=1 beg1=%d end1=%d beg2=%d end2=%d beg3=%d end3=%d < %s > %s/temp%d.h1" % (randpadin,randpadin,randpadin,randpadin,randpadin,randpadin,inputvel,writepath,nodeId)
cmd="%s; %s;" % (cmd0,cmd1)
print(cmd)
subprocess.call(cmd,shell=True)

# Get the inputvel grid spacing and dimensions
getds = "Get < %s/temp%d.h1 parform=n d1 " % (writepath,nodeId)
aa=commands.getstatusoutput(getds)
d1=float(aa[1])
getds = "Get < %s/temp%d.h1 parform=n n1 " % (writepath,nodeId)
aa=commands.getstatusoutput(getds)
n1=int(aa[1])
getds = "Get < %s/temp%d.h1 parform=n n2 " % (writepath,nodeId)
aa=commands.getstatusoutput(getds)
n2=int(aa[1])
getds = "Get < %s/temp%d.h1 parform=n n3 " % (writepath,nodeId)
aa=commands.getstatusoutput(getds)
n3=int(aa[1])

# New parameters
d1new=d1*scale

n1s=n1-2*randpadin
n2s=n2-2*randpadin
n3s=n3-2*randpadin

# Interpolate and find the random boundaries
cmdI1="Interp type=0 maxsize=2000 d1out=%f d2out=%f d3out=%f < %s/temp%d.h1 > %s " % (d1new,d1new,d1new,writepath,nodeId,coarsevelIN)
cmd2="%s/randomB.x modelIn=%s par=%s pad=%d seed=%d modelOut=%s" % (binpath,coarsevelIN,genpar,randpad,nodeId,coarsevelOUT)
cmdI2="Interp type=0 maxsize=2000 d1out=%f d2out=%f d3out=%f n1out=%s n2out=%s n3out=%s < %s > %s0 " % (d1,d1,d1,n1,n2,n3,coarsevelOUT,coarsevelOUT)
cmd3="Add scale=1,-1 < %s0 %s/temp%d.h0 > %s1 " % (coarsevelOUT,writepath,nodeId,coarsevelOUT) # No RBC at the top
# Cut
cut="Window3d f1=%s n1=%s f2=%s n2=%s f3=%s n3=%s < %s1 > %s2" % (randpadin,n1s,randpadin,n2s,randpadin,n3s,coarsevelOUT,coarsevelOUT)
# Pad with zeros
pad="Pad extend=0 beg1=%d end1=%d beg2=%d end2=%d beg3=%d end3=%d < %s2 > %s3" % (randpadin,randpadin,randpadin,randpadin,randpadin,randpadin,coarsevelOUT,coarsevelOUT)

#++++++++++++++++++++++++++++++++++++++++++++++++++++
#CHOOSE ONE VERSION BELOW AND COMMENT THE OTHER

# # Subtract again and make hard top BC
# subtract="Add scale=1,-1 %s1 %s3 | Window3d f1=%s > %s" % (coarsevelOUT,coarsevelOUT,randpadin,outputFileName)

# Subtract again and leave RBC zone on top
subtract="Add scale=1,-1 %s1 %s3 > %s" % (coarsevelOUT,coarsevelOUT,outputFileName)
#++++++++++++++++++++++++++++++++++++++++++++++++++++

cmd="%s; %s; %s; %s; %s; %s; %s;" % (cmdI1,cmd2,cmdI2,cmd3,cut,pad,subtract)
#cmd="%s; %s; %s; %s; %s; %s; %s; rm -f %s/temp%d.h* Pad* %s* %s*" % (cmdI1,cmd2,cmdI2,cmd3,cut,pad,subtract,writepath,nodeId,coarsevelOUT,coarsevelIN)
print(cmd)
subprocess.call(cmd,shell=True)









