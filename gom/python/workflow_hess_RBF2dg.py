#!/usr/local/bin/python

import copy
import random
import os
import sepbase
import tempfile
import time
import unittest
import math
import subprocess
import numpy as np
from numpy.linalg import inv
from batch_task_executor import *
import pbs_util
from main_functions3d import *
from inversion2d import *
from linesearchRBF_adaptive import *
from source_inversion2D import *


# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

if __name__ == '__main__':
    print "Run BatchTaskExecutor_test with params:", sys.argv
    # Check basic environment variable setup.
    assert 'SEP' in os.environ, '!Environment var SEP is not set yet, should have your seplib enviroment set up first! Check paths like /opt/SEP/'
    assert 'RSF' in os.environ, '!Environment var RSF is not set yet, should have your RSF environment set up first! Check paths like /opt/RSF'
    eq_args_from_cmdline, args = sepbase.parse_args(sys.argv)
    param_reader = pbs_util.JobParamReader(eq_args_from_cmdline)

    #------ INITIALIZE SOME STUFF ----------------------
    dict_args = param_reader.dict_args
    levelset_iter = int(dict_args['levelset_iter'])
    dpIncrease=1.25
    betaDecrease=0.75
    Ntries = 10
    debug = True
    LStomoupd=False
    LSphiupd=True

    inversionname = param_reader.dict_args['inversionname']
    vel_path = param_reader.dict_args['vel_path']
    velback  = param_reader.dict_args['velback']
    syn_path = param_reader.dict_args['syn_path']
    res_path = param_reader.dict_args['res_path']
    rtm_path = param_reader.dict_args['rtm_path']
    phi_path = param_reader.dict_args['phi_path']
    masksalt = param_reader.dict_args['masksalt']
    maskvelb = param_reader.dict_args['maskvelb']
    pathtemp = param_reader.dict_args['pathtemp']
    wavelet  = param_reader.dict_args['wavelet']
    obs_path = param_reader.dict_args['obs_path']
    sparsemodel = param_reader.dict_args['sparsemodel']
    watermask = param_reader.dict_args['watermask']

    phigrad = param_reader.dict_args['phigrad']
    tomograd = param_reader.dict_args['tomograd']
    pathtemphess = param_reader.dict_args['pathtemphess']
    hess_out_path = param_reader.dict_args['hess_out_path']

    hessian=bool(int(param_reader.dict_args['hessian']))
    RBF=bool(int(param_reader.dict_args['RBF']))
    masking=bool(int(param_reader.dict_args['masking']))

    # INITIALIZING THE MAIN FUNCTION SPACES. FUNCTIONS THAT CALL OTHER FUNCTIONS RELY ON THESE OBJECT INSTANCES
    main = MAIN_FUNCTIONS(param_reader)
    wave_modeling = make_MODELED_DATA2d(param_reader)
    born_modeling =make_BORN(param_reader)
    mainLS = MAIN_LS_FUNCTIONS(debug, param_reader, wave_modeling, main, inversionname, Ntries, LStomoupd, LSphiupd)
    inversion = inversion(param_reader,inversionname)
    souInv=SOURCE_INVERSION(param_reader,'sourceInv')



    ###################################################################################################
    #----------- BEGIN LEVELSET ITERATIONS ------------------------------------------------------------
    for i in range(0, levelset_iter):
        print("###################################################################################################")
        print("########################      LEVELSET ITERATION   %s / %s   ######################################") % (i, (levelset_iter - 1))
        print("###################################################################################################")

        #------- DO SOURCE INVERSION -------------------------------------------------
        souInv.souInvMAIN(param_reader, True, wavelet, obs_path)

        #------- MAKE SYNTHETIC DATA AND RESIDUAL -------------------------------------------------
        resname="residual"
        jobtype="rtm"
        wave_modeling.MODELED_DATA(param_reader, debug, resname, jobtype, vel_path, False, wavelet, syn_path)

        #-------- CALCULATE THE OBJECTIVE FUNCTION VALUE  ---------------------------------------------
        new_objfuncval = main.CALC_OBJ_VAL(debug, resname)
        print(">>>>>>>>>>>> Objective function value: %s" % (new_objfuncval))

        print(" ====================================================================================")
        #------- MAKE FULL RTM GRADIENT ------------------------------------------
        gradtag = 'gradient'
        printwavefield=False
        dataAlreadySplit=True
        born_modeling.BORN_run(param_reader, gradtag, False, debug, True, rtm_path, resname, printwavefield, dataAlreadySplit)


        #------- MAKE MASKS FOR D OPERATOR ---------------------------------------
        masksaltTMP=masksalt+'_TMP'
        addguide=True
        main.MAKE_MASKS_RBFg(debug,phi_path,masksaltTMP,maskvelb,addguide=addguide)
        main.PAD2D_ABC(debug, masksaltTMP, masksalt)

        #------- MAKE SCALING ----------------------------------------------------
        main.MAKE_SCALING2d(debug)

        print(" ====================================================================================")
        
        #--------- HESSIAN INVERSION ITERATION --------------------------------
        phiANDvelb = False
        phigradTMP1 = phigrad+'_TMP1'
        phigradTMP2 = phigrad+'_TMP2'
        rtm_pathRBF = rtm_path+'_RBFsize'
        symmetric = True

        if (hessian):
            if (RBF):
                print("==========  RUNING RBF GN HESS ============== \n")
                main.DELTAP_BEFORE_RBF2d(debug,rtm_path,sparsemodel)
                inversion.hessMAIN(param_reader, debug, True, True, masking, phiANDvelb, "junk", symmetric, sparsemodel, hess_out_path)
                main.DELTAP_AFTER_RBF2d(debug,hess_out_path,phigrad,tomograd)
            else:
                if (masking):
                    main.HADAMARD_PRODUCT(debug,rtm_path,masksalt,phigradTMP1)
                    inversion.hessMAIN(param_reader, debug, True, False, masking, phiANDvelb, "junk", symmetric, phigradTMP1, hess_out_path)
                    main.HADAMARD_PRODUCT(debug,hess_out_path,masksalt,phigradTMP2)
                else:
                    inversion.hessMAIN(param_reader, debug, True, False, masking, phiANDvelb, "junk", symmetric, rtm_path, hess_out_path)
                    main.HADAMARD_PRODUCT(debug,hess_out_path,masksalt,phigradTMP2)
                main.WINDOW2D_PAD(debug, phigradTMP2, phigrad)
        else:
            if (RBF):
                print("==========  RUNING RBF GN HESS ============== \n")
                main.DELTAP_BEFORE_RBF2d(debug,rtm_path,sparsemodel)
                main.SCALE(debug, sparsemodel, hess_out_path, -1.0)
                main.DELTAP_AFTER_RBF2d(debug,hess_out_path,phigrad,tomograd)
            else:
                main.HADAMARD_PRODUCT(debug,rtm_path,masksalt,phigradTMP1)       
                main.SCALE(debug, phigradTMP1, phigradTMP2, -1.0)
                main.WINDOW2D_PAD(debug, phigradTMP2, phigrad)


        #----- Expand the phigrad to full size ----------------------------------------------------
        main.SCALE(debug, phigrad, tomograd, 1.0) # Doesnt really matter since we dont update the background these days

        #----- Calc maxbeta ----------------------------------------------------
        maxbeta = main.CALC_MAXBETA_RBF(debug,1.0,phigrad,phi_path)

        #------- LINE SEARCH ---------------------------------------------------
        final_alpha,final_beta=mainLS.LINE_SEARCH(maxbeta, new_objfuncval, phigrad, tomograd, betaDecrease)

        #------- SAVE THE OUTPUTS FROM THIS ITERATION  -------------------------
        main.SAVE_INTERMEDIATE_FILES(debug,inversionname)
        main.SAVE_INTERMEDIATE_VALUES(debug, new_objfuncval, maxbeta, final_alpha, final_beta)

        #------- BUILD MODEL FROM NEW PHI AND MAXBETA  -------------------------
        newphi_path="%s_GUESS_%s" % (phi_path,i)
        newvelback="%s_GUESS_%s" % (velback,i)
        mainLS.UPDATE_MODEL_LS3d(debug, final_alpha, final_beta, phigrad, tomograd, vel_path, newphi_path,newvelback)
        main.COPY(debug,newphi_path,phi_path)

        #-------- CLEAN THE WORKING DIRECTORY   --------------------------------
        main.CLEAN_UP(debug,inversionname)
