#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#
#   PROGRAM MAKES RANDOM ABCS FOR EACH NODE
#
#XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


#!/usr/local/bin/python
import sys
import os
import math
import commands
import sepbase
import sep2npy
import numpy as np
import subprocess


# Read the inputs
if len(sys.argv) < 2:
    print('Error: Not enough input args')
    sys.exit()
nodeId = int(sys.argv[1])
pad = sys.argv[2]
binpath = sys.argv[3]
genpar = sys.argv[4]
inputvel = sys.argv[5]
outputFileName = sys.argv[6]
writepath = sys.argv[7]
randpad = int(sys.argv[8])

cmd0="Pad extend=0 beg1=%s end1=%s beg2=%s end2=%s < %s > %s/temp%d.h0" % (randpad,randpad,randpad,randpad,inputvel,writepath,nodeId)
cmd1="Pad extend=1 beg1=%s end1=%s beg2=%s end2=%s < %s > %s/temp%d.h1" % (randpad,randpad,randpad,randpad,inputvel,writepath,nodeId)
cmd2="%s/randomB.x modelIn=%s/temp%d.h1 par=%s pad=%d seed=%d modelOut=%s/temp%d.h2" % (binpath,writepath,nodeId,genpar,randpad,nodeId,writepath,nodeId)
# # HARD TOP TO SIMULATE FREE SURFACE
# cmd3="Add scale=1,-1 < %s/temp%d.h2 %s/temp%d.h0 | Window3d f1=%s > %s" % (writepath,nodeId,writepath,nodeId,randpad,outputFileName) # No RBC at the top
# RBC TOP
cmd3="Add scale=1,-1 < %s/temp%d.h2 %s/temp%d.h0 > %s" % (writepath,nodeId,writepath,nodeId,outputFileName) # Has RBC at the top
cmd="%s; %s; %s; %s" % (cmd0,cmd1,cmd2,cmd3)
print(cmd)
subprocess.call(cmd,shell=True)









