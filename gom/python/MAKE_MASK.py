#================================================================================================
#       Program to make salt boundary mask
#
#       Usage:
#               >> python MAKE_MASK.py input=guess_phi.H output=masksalt.H
#
#
#       Taylor Dahlke, 1/22/2018 taylor@sep.stanford.edu
#================================================================================================

#!/usr/bin/python

import subprocess
import sepbasic
import sys
import math, copy

# Read in the input parameters.
eq_args_from_cmdline, args = sepbasic.parse_args(sys.argv)
pars=sepbasic.RetrieveAllEqArgs(eq_args_from_cmdline)
masksalt=pars["masksalt"]
infile=pars["infile"]
width=pars["width"]
binpath=pars["binpath"]
guide=pars["guide"]


# Get the norm of the gradient of phi (the phi slope steepness).
cmd0a="%s/CLIP.x clipval=0.0 replacelessthan=0 replaceval=4500.0 < %s > %s0a" % (binpath,infile,masksalt)
cmd0b="%s/CLIP.x clipval=4400.0 replaceval=2000.0 < %s0a | Scale > %s0b" % (binpath,masksalt,masksalt)
cmd1="%s/PHI_SLOPE.x verbose=1 < %s0b > %s1" % (binpath,masksalt,masksalt)
cmd2="%s/CLIP.x clipval=0.1 replaceval=0.0 < %s1 | Scale > %s2" % (binpath,masksalt,masksalt)
# Smooth that slope, add interpreter guide to it, then repeat.
cmd3="Smooth rect1=%s rect2=%s rect3=%s < %s2 | Scale > %s3" % (width,width,width,masksalt,masksalt)

# Use the sides or not
cmdcommon="%s; \n %s; \n %s; \n %s; \n %s;" % (cmd0a,cmd0b,cmd1,cmd2,cmd3)
# cmdcommon="%s; \n %s; \n %s; \n %s; \n %s; Solver_ops file1=%s3 op=zero; " % (cmd0a,cmd0b,cmd1,cmd2,cmd3,masksalt)


# Use the edges or not (depending on if it was in the input)
if 'edges' in pars:
	cmd3b="Scale < %s > %s_scaled" % (guide,guide)
	# cmd4="Math file1=%s3 file2=%s_scaled exp='file1*file2' > %s4" % (masksalt,guide,masksalt)
	# cmd4="Add %s3 %s_scaled > %s4" % (masksalt,guide,masksalt)
	cmd4="Cp %s3 %s4" % (masksalt,masksalt)
	cmdguide="%s; \n %s; \n " % (cmd3b,cmd4)
else:
	cmd3b="Scale < %s > %s_scaled" % (guide,guide)
	# cmd4="Cp %s_scaled %s4" % (guide,masksalt)
	cmd4="Add %s3 %s_scaled > %s4" % (masksalt,guide,masksalt)
	cmdguide="%s; \n %s; \n " % (cmd3b,cmd4)
	# cmdguide="Cp %s3 %s4;" % (masksalt,masksalt)


cmd5="%s/CLIP.x clipval=0.3  replacelessthan=0  < %s4 | Scale > %s5" % (binpath,masksalt,masksalt)
cmd6="Smooth rect1=%s rect2=%s rect3=%s < %s5 | Scale > %swb" % (width,width,width,masksalt,masksalt)


# Use the watermask or not (depending on if it was in the input)
if 'watermask' in pars:
	watermask=pars["watermask"]
	wbtm="Math file1=%s file2=%swb exp='file1*file2*file2*file2' > %slow;" % (watermask,masksalt,masksalt)
else:
	wbtm="Cp %swb %slow;" % (masksalt,masksalt)


rmlow="%s/CLIP.x clipval=0.1 replaceval=0.0 < %slow > %s" % (binpath,masksalt,masksalt)
cmdALL="%s %s \n %s; \n %s; %s \n %s;" % (cmdcommon,cmdguide,cmd5,cmd6,wbtm,rmlow)
print("\n\n %s" % (cmdALL))
subprocess.call(cmdALL,shell=True)
rmcmd="rm -f %s0a %s0b %s1 %s2 %s3 %s4 %swb %slow" % (masksalt,masksalt,masksalt,masksalt,masksalt,masksalt,masksalt,masksalt)
subprocess.call(rmcmd,shell=True)




