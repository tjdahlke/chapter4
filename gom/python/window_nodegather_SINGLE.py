# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
#
#   PROGRAM GRABS NODE GATHERS AND DOES PROCESSING ON THEM
#
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


#!/usr/local/bin/python
import sys
import os
import math
import commands
import sepbase
import sep2npy
import numpy as np
import subprocess


# Read the inputs
if len(sys.argv) < 2:
    print('Error: Not enough input args')
    sys.exit()
nodeId = int(sys.argv[1])
obsdata = sys.argv[2]
outputfile = sys.argv[3]


print("----------------------------------------")

# Grab obs data for node
cmdW="Window_key synch=1 key1='nodeId' mink1=%d maxk1=%d < %s > %s hff=%s@@" % (nodeId,nodeId,obsdata,outputfile,outputfile)
print(cmdW)
commands.getstatusoutput(cmdW)





