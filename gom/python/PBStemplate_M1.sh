#!/bin/tcsh
### Each node on cees-mazama has 24 cores.
#PBS -l nodes=1:ppn=2
#PBS -q default
#PBS -V
#PBS -j oe
##PBS -l walltime=01:0:00
