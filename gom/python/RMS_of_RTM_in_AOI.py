

# Find RTM images that are making the biggest amplitude contributions in the given AOI
# Taylor Dahlke 8/24/2018


import sepbase
import sep2npy
import sys,os,math
import commands
import time
import matplotlib.pyplot as plt
import numpy as np
from operator import itemgetter


# Function to check is file exists
def check_file(file):
	# Check if file exists
	cmd0 = "ls %s " % file
	stat0,out0=commands.getstatusoutput(cmd0)
	if (int(stat0) != 0):  # File does not exist
		print("File does not exist")
		return 0
	else:
		return 1





# MAIN PROGRAM
if __name__ == '__main__':
	eq_args_from_cmdline,args = sepbase.parse_args(sys.argv)
	dict_args = eq_args_from_cmdline

	################  PARAMETERS ################
	# List of nodes and relative positions
	recpos = dict_args['recpos']
	# PBS template
	PBStemp = dict_args['PBStemp']
	# Output path for PBS stuff
	outpath = dict_args['outpath']
	runpath = dict_args['runpath']
	pypath = dict_args['pypath']
	binpath = dict_args['binpath']
	cutoffpercentL = float(dict_args['cutoffpercentL'])
	cutoffpercentH = float(dict_args['cutoffpercentH'])
	stackedfile = dict_args['stackedfile']
	idealAOI = dict_args['idealAOI']

	# Initialize
	reclist=[]
	cmd1="less %s | wc -l" % (recpos)
	stat1,numNodes=commands.getstatusoutput(cmd1)
	numNodes=int(numNodes)
	recList = list(open(recpos))
	someNodesNotDone=True
	numTries=5


	# #--------------------------------------------
	# # First make the AOI files
	# tries=0
	# while(someNodesNotDone):

	# 	someNodesNotDone=False

	# 	for ii in range(0,numNodes):

	# 		nodeId=int(str(recList[ii]).split()[0])
	# 		aoifile='%s/aoi_RTM_%d.H' % (outpath,nodeId)

	# 		makeAOI = 'make %s' % (aoifile)

	# 		# Check if the file is already made
	# 		if (sepbase.CheckSephFileError(aoifile)==0):
	# 			print("File: %s already exists \n" % (aoifile))
	# 		else:
	# 			someNodesNotDone=True
	# 			# Make a qsub file
	# 			pbsfile="%s/PBS_AOI_%d.sh" % (outpath,nodeId)

	# 			cmdp3="echo '#PBS -N pbsjob-%s' >> %s" % (nodeId,pbsfile)
	# 			cmdp4="echo '#PBS -d %s' >> %s" % (outpath,pbsfile)
	# 			cmdp5="echo 'setenv DATAPATH %s/' >> %s" % (outpath,pbsfile)
	# 			cmdp="%s; %s; %s" % (cmdp3, cmdp4, cmdp5)
	# 			print(cmdp)
	# 			stat2,junk2=commands.getstatusoutput(cmdp)

	# 			cmd2="cp %s %s; echo '%s' >> %s " % (PBStemp,pbsfile,makeAOI,pbsfile)
	# 			stat1,junk1=commands.getstatusoutput(cmd2)
	# 			# Submit the qsub file
	# 			cmd3="qsub -d %s -o %s.O -e %s.E %s" % (runpath,pbsfile,pbsfile,pbsfile)
	# 			print(cmd3)
	# 			stat2,junk2=commands.getstatusoutput(cmd3)

	# 	if (someNodesNotDone):
	# 		print("Waiting for josb to finish")
	# 		time.sleep(60)

	# 	# Try some checks
	# 	tries=tries+1
	# 	if (tries>numTries):
	# 		someNodesNotDone=False

	# #--------------------------------------------
	# # Then get the RMS values for each one
	# tries=0
	# someNodesNotDone=True
	# while(someNodesNotDone):

	# 	someNodesNotDone=False

	# 	for ii in range(0,numNodes):

	# 		nodeId=int(str(recList[ii]).split()[0])
	# 		rmslogfile="%s/rmsvalue_%d.log" % (outpath,nodeId)
	# 		aoifile='%s/aoi_RTM_%d.H' % (outpath,nodeId)

	# 		rmsAOI  = 'Attr param=1 want=rms < %s | Get parform=n rms > %s' % (aoifile,rmslogfile)

	# 		# Check if the file is already made
	# 		if (check_file(rmslogfile)==1):
	# 			print("File: %s already exists \n" % (rmslogfile))
	# 		else:
	# 			someNodesNotDone=True
	# 			# Make a qsub file
	# 			pbsfile="%s/PBS_RMS_%d.sh" % (outpath,nodeId)
	# 			cmd2="cp %s %s; echo '%s' >> %s  " % (PBStemp,pbsfile,rmsAOI,pbsfile)
	# 			stat1,junk1=commands.getstatusoutput(cmd2)
	# 			# Submit the qsub file
	# 			cmd3="qsub -o %s.O -e %s.E %s" % (pbsfile,pbsfile,pbsfile)
	# 			print(cmd3)
	# 			stat2,junk2=commands.getstatusoutput(cmd3)

	# 	# Try some checks
	# 	tries=tries+1
	# 	if (tries>numTries):
	# 		someNodesNotDone=False


	# #------------------------------------------------------
	# # Then get the dot product with the ideal version
	# tries=0
	# someNodesNotDone=True
	# while(someNodesNotDone):

	# 	someNodesNotDone=False

	# 	for ii in range(0,numNodes):

	# 		nodeId=int(str(recList[ii]).split()[0])
	# 		rmslogfile="%s/dotvalue_%d.log" % (outpath,nodeId)
	# 		aoifile='%s/aoi_RTM_%d.H' % (outpath,nodeId)

	# 		dotAOI  = "python %s/calc_norm2.py %s %s %s " % (pypath,aoifile,idealAOI,rmslogfile)

	# 		# Check if the file is already made
	# 		if (check_file(rmslogfile)==1):
	# 			print("File: %s already exists \n" % (rmslogfile))
	# 		else:
	# 			someNodesNotDone=True
	# 			# Make a qsub file
	# 			pbsfile="%s/PBS_DOT_%d.sh" % (outpath,nodeId)
	# 			cmd2="cp %s %s" % (PBStemp,pbsfile)
	# 			stat1,junk1=commands.getstatusoutput(cmd2)
	# 			cmdp5="echo 'setenv DATAPATH %s/' >> %s" % (outpath,pbsfile)
	# 			stat1,junk1=commands.getstatusoutput(cmdp5)
	# 			cmd2="echo '%s' >> %s " % (dotAOI,pbsfile)
	# 			stat1,junk1=commands.getstatusoutput(cmd2)

	# 			# Submit the qsub file
	# 			cmd3="qsub -o %s.O -e %s.E %s" % (pbsfile,pbsfile,pbsfile)
	# 			print(cmd3)
	# 			stat2,junk2=commands.getstatusoutput(cmd3)

	# 	if (someNodesNotDone):
	# 		print("Waiting for josb to finish")
	# 		time.sleep(200)

	# 	# Try some checks
	# 	tries=tries+1
	# 	if (tries>numTries):
	# 		someNodesNotDone=False

	# #--------------------------------------------------------------
	# # Make histogram of DOT values of each node in the AOI

	# rmsList=[]

	# for ii in range(0,numNodes):

	# 	nodeId=int(str(recList[ii]).split()[0])
	# 	# rmslogfile="%s/rmsvalue_%d.log" % (outpath,nodeId)
	# 	rmslogfile="%s/dotvalue_%d.log" % (outpath,nodeId)

	# 	rmsVal = float(list(open(rmslogfile))[0])
	# 	rmsList.append(rmsVal)

	# rmsArray=np.asarray(rmsList)
	## rmsArray=rmsArray*(-1.0)
	# plt.hist(rmsArray, bins='auto')
	# plt.title("Dot product value for each node gather with ideal image for the inclusion AOI")
	# plt.show()


	# #--------------------------------------------------------------
	# # List of nodes that are important RMS
	# impNodes=[]
	# for ii in range(0,numNodes):
	# 	nodepair=()
	# 	nodeId=int(str(recList[ii]).split()[0])
	# 	rmslogfile="%s/rmsvalue_%d.log" % (outpath,nodeId)
	# 	rmsVal = float(list(open(rmslogfile))[0])
	# 	nodepair=(nodeId,rmsVal)
	# 	impNodes.append(nodepair)


	# sortedByRMS=sorted(impNodes, key=itemgetter(1),reverse=True)

	# print(sortedByRMS)
	# numNodesToGrabL=int((cutoffpercentL)*numNodes/100)
	# numNodesToGrabH=int((cutoffpercentH)*numNodes/100)
	# mostImportantNodes=sortedByRMS[numNodesToGrabH:numNodesToGrabL]


	#--------------------------------------------------------------
	# List of nodes that are important DOT PRODUCT
	impNodes=[]
	for ii in range(0,numNodes):
		nodepair=()
		nodeId=int(str(recList[ii]).split()[0])
		rmslogfile="%s/dotvalue_%d.log" % (outpath,nodeId)
		rmsVal = float(list(open(rmslogfile))[0])
		if (rmsVal<0.0):
			nodepair=(nodeId,rmsVal)
			impNodes.append(nodepair)


	sortedByDOT=sorted(impNodes, key=itemgetter(1),reverse=True)

	print(sortedByDOT)
	numPosNodes=len(sortedByDOT)
	numNodesToGrabL=int((cutoffpercentL)*numPosNodes/100)
	numNodesToGrabH=int((cutoffpercentH)*numPosNodes/100)
	mostImportantNodes=sortedByDOT[numNodesToGrabH:numNodesToGrabL]


	#--------------------------------------------------------------
	# Write files to a list
	# recursiveFileList='recursiveFileListRMS.txt'
	recursiveFileList='recursiveFileListDOT.txt'
	cmd1="rm %s" % (recursiveFileList)
	stat2,junk2=commands.getstatusoutput(cmd1)
	for nodepair in mostImportantNodes:
		nodeId=nodepair[0]
		filename='wrk/RTM3d-%s.H' % (nodeId)
		writefilename="echo '%s' >> %s" % (filename,recursiveFileList)
		stat2,junk2=commands.getstatusoutput(writefilename)

	#--------------------------------------------------------------
	# Recursively add those files now
	cmd1='python %s/recursive_add_file_list.py %s %s "%s" > recurseiveADD.log' % (pypath,binpath,stackedfile,recursiveFileList)
	print(cmd1)
	stat2,junk2=commands.getstatusoutput(cmd1)















